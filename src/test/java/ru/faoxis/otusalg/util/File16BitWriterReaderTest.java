package ru.faoxis.otusalg.util;

import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class File16BitWriterReaderTest {

    @Test
    public void testWriteAndRead() throws IOException {
        String fileName = "./test.bin";

        File16BitWriter writer = new File16BitWriter(fileName);
        writer.write(1);
        writer.write(0xFD);
        writer.write(0xDE);
        writer.write(0);
        writer.write(0x5555);
        writer.write(0xFFFF);
        writer.close();

        File16BitReader reader = new File16BitReader(fileName);
        assertEquals(reader.read(), 1);
        assertEquals(reader.read(), 0xFD);
        assertEquals(reader.read(), 0xDE);
        assertEquals(reader.read(), 0);
        assertEquals(reader.read(), 0x5555);
        assertEquals(reader.read(), 0xFFFF);
        assertEquals(reader.read(), -1);
        assertEquals(reader.read(), -1);
        assertFalse(reader.isAvailable());

        reader.close();

        Files.delete(Paths.get(fileName));
    }

}