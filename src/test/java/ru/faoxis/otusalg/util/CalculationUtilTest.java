package ru.faoxis.otusalg.util;

import org.junit.Test;

import static org.junit.Assert.*;

public class CalculationUtilTest {

    @Test
    public void testTwoSeconds() {
        int iterations = 2;
        int seconds = 1;

        long res = CalculationUtil.calculateOperationTime(index -> {
            try {
                Thread.sleep(seconds * 1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }, iterations);

        int expectedMs = iterations * seconds * 1000;
        int rangeMs = 10;
        assertTrue(res > expectedMs - rangeMs);
        assertTrue(res < expectedMs + rangeMs);

    }

}