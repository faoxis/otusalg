package ru.faoxis.otusalg.util;

import org.junit.Test;
import ru.faoxis.otusalg.mycollection.mylist.mutable.List;

public class PerformanceResultPrinterTest {

    @Test
    public void test() {
        PerformanceResultPrinter performanceResultPrinter = new PerformanceResultPrinter(40, 20, i -> i + 1);
        performanceResultPrinter.addResult("label1", List.of(1L, 2L, 3L, 4L, 5L));
        performanceResultPrinter.addResult("label2", List.of(1L, 2L, 3L, 4L, 5L));
        performanceResultPrinter.addResult("label3", List.of(1L, 2L, 3L, 4L, 5L));
        System.out.println(performanceResultPrinter.getMarkdownTable());
    }

}