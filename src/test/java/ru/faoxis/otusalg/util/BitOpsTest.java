package ru.faoxis.otusalg.util;

import org.junit.Test;
import ru.faoxis.otusalg.mycollection.mylist.mutable.List;

import static org.junit.Assert.*;

public class BitOpsTest {

    @Test
    public void testToString() {
        BitOps bitOps = new BitOps(8);
        for (int i = 0; i < 8; i++) {
            if (i % 2 == 0) {
                bitOps.set(i);
            }
        }
        assertEquals("01010101", bitOps.toString());
    }

    @Test
    public void testSetAll() {
        BitOps bitOps = new BitOps(1_000_000);
        bitOps.setAll();
        for (int i = 0; i < 1_000_000; i++) {
            assertTrue(bitOps.get(i));
        }
    }

    @Test
    public void resetAll() {
        BitOps bitOps = new BitOps(1_000_000);
        bitOps.setAll();
        bitOps.resetAll();
        for (int i = 0; i < 1_000_000; i++) {
            assertFalse(bitOps.get(i));
        }
    }

    @Test
    public void testSetAndGet() {
        BitOps bitOps = new BitOps(1000);
        List<Integer> trueIndexes = List.of(0, 31, 32, 33, 63, 64, 65, 555, 999);
        for (int i = 0; i < trueIndexes.size(); i++) {
            bitOps.set(trueIndexes.get(i));
        }

        for (int i = 0; i < bitOps.getSize(); i++) {
            if (trueIndexes.contains(i)) {
                assertTrue(bitOps.get(i));
            } else {
                assertFalse(bitOps.get(i));
            }
        }
    }

}