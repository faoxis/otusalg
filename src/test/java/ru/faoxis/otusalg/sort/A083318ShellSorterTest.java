package ru.faoxis.otusalg.sort;

import ru.faoxis.otusalg.sort.shell.A083318ShellSorter;

public class A083318ShellSorterTest extends SorterTest {

    @Override
    public Sorter getSorter() {
        return new A083318ShellSorter();
    }
}