package ru.faoxis.otusalg.sort.quick;


import ru.faoxis.otusalg.sort.Sorter;
import ru.faoxis.otusalg.sort.SorterTest;


public class QuickSorterRandomTest extends SorterTest {

    @Override
    public Sorter getSorter() {
        return new QuickSorter(QuickSortMode.RANDOM_PIVOT);
    }
}