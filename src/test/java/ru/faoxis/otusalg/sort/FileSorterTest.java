package ru.faoxis.otusalg.sort;

import org.junit.Test;
import ru.faoxis.otusalg.mycollection.mylist.mutable.ArrayList;
import ru.faoxis.otusalg.mycollection.mylist.mutable.FactorList;
import ru.faoxis.otusalg.sort.FileSorter;
import ru.faoxis.otusalg.sort.InsertionSorter;
import ru.faoxis.otusalg.util.File16BitReader;
import ru.faoxis.otusalg.util.FileGenerator;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.ExecutionException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class FileSorterTest {

    @Test
    public void testSorting() throws IOException, URISyntaxException, ExecutionException, InterruptedException {
        String directory = ".";
        String filename = "test";
        Path pathToFile = Paths.get(directory + File.separator + filename);

        int numbers = 1_000_000;

        FileGenerator.createFile(pathToFile.toString(), numbers);
        FileSorter fileSorter = new FileSorter(directory, filename, new InsertionSorter());

        fileSorter.clearOutputDirectory();
        String sortedFileName = fileSorter.sort(1024);
        String sortedFilePath = directory + File.separator + "output" + File.separator + sortedFileName;
        File16BitReader reader = new File16BitReader(sortedFilePath);

        ArrayList<Integer> sorted = new FactorList<Integer>();
        while(reader.isAvailable()) {
            if (sorted.size() == 512) {
                System.out.println();
            }
            sorted.add(reader.read());
        }
        reader.close();
        assertEquals(numbers, sorted.size());
        for (int i = 0; i < sorted.size() - 2; i++) {
            if (!(sorted.get(i) <= sorted.get(i + 1))) {
                System.out.println("sorted[i]=" + sorted.get(i));
                System.out.println("sorted[i + 1]=" + sorted.get(i + 1));
                System.out.println("i=" + i);
            }
            assertTrue(sorted.get(i) <= sorted.get(i + 1));
        }
    }

}