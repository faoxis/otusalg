package ru.faoxis.otusalg.sort;

public class BubbleSorterTest extends SorterTest {

    @Override
    public Sorter getSorter() {
        return new BubbleSorter();
    }
}