package ru.faoxis.otusalg.sort;

import ru.faoxis.otusalg.sort.shell.A168604ShellSorter;

public class A168604ShellSorterTest extends SorterTest {

    @Override
    public Sorter getSorter() {
        return new A168604ShellSorter();
    }
}
