package ru.faoxis.otusalg.sort;

import org.junit.Test;
import ru.faoxis.otusalg.sort.O1Sorters.CountingSorter;

import java.util.Arrays;
import java.util.Random;

import static org.junit.Assert.*;

public class CountingSorterTest {

    private Random random = new Random();

    @Test
    public void simpleTestSorting() {
        int bound = 1_000;
        int size = 1_000_000;
        int[] source = createArray(bound, size);

        int[] sorted = Arrays.copyOf(source, source.length);
        CountingSorter sorter = new CountingSorter(bound);
        sorter.sort(sorted);

        for (int i = 0; i < size; i++) {
            assertTrue(containsIn(source, sorted[i]));
        }

        for (int i = 0; i < size - 1; i++) {
            if (sorted[i] > sorted[i + 1]) {
                System.out.println("element " + sorted[i] + " bigger then " + sorted[i + 1]);
            }
            assertTrue(sorted[i] <= sorted[i + 1]);
        }
    }

    private boolean containsIn(int[] arr, int element) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == element)
                return true;
        }
        return false;
    }

    private int[] createArray(int bound, int size) {
        int[] arr = new int[size];
        for (int i = 0; i < size; i++) {
            arr[i] = random.nextInt(bound);
        }
        return arr;
    }

}