package ru.faoxis.otusalg.sort;

import org.junit.Ignore;
import org.junit.Test;

import java.util.Random;
import java.util.function.Consumer;

import static org.junit.Assert.assertTrue;

@Ignore
public abstract class SortTest {


    protected abstract void makeSort(int[] mass) throws InterruptedException;

    @Test
    public void sort() throws InterruptedException {
        int[] mass = new Random().ints(1000, 0, 4242).toArray();
        makeSort(mass);
        for (int i = 0; i < mass.length - 2; i++) {
            assertTrue(mass[i] <= mass[i + 1]);
        }

    }

}
