package ru.faoxis.otusalg.sort.O1Sorters;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import ru.faoxis.otusalg.sort.Sorter;

import java.util.Random;

import static org.junit.Assert.assertTrue;

@Ignore
public abstract class O1SorterTest {

    private int[] randomArray;
    private Random random = new Random();

    protected abstract Sorter getSorter();
    protected abstract int getBound();
    protected abstract int getArraySize();

    @Before
    public void setUp() {
        randomArray = random
            .ints(getArraySize(), 0, getBound())
            .toArray();
    }

    @Test
    public void testSorting() {
        int[] sortedArray = new int[getArraySize()];
        System.arraycopy(randomArray, 0, sortedArray, 0, getArraySize());

        Sorter sorter = getSorter();
        sorter.sort(sortedArray);

        for (int e : randomArray) {
            assertTrue(containsIn(sortedArray, e));
        }

        for (int i = 0; i < getArraySize() - 2; i++) {
            assertTrue(sortedArray[i] <= sortedArray[i + 1]);
        }

    }

    private boolean containsIn(int[] arr, int elem) {
        for (int e : arr) {
            if (e == elem) {
                return true;
            }
        }
        return false;
    }

}
