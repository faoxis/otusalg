package ru.faoxis.otusalg.sort.O1Sorters;

import ru.faoxis.otusalg.sort.Sorter;

import static org.junit.Assert.*;

public class CountingSorterTest extends O1SorterTest {

    private int bound = 1000;

    @Override
    protected Sorter getSorter() {
        return new CountingSorter(bound);
    }

    @Override
    protected int getBound() {
        return bound;
    }

    @Override
    protected int getArraySize() {
        return 100_000;
    }
}