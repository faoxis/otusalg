package ru.faoxis.otusalg.sort.O1Sorters;

import org.junit.Before;
import org.junit.Test;
import ru.faoxis.otusalg.sort.Sorter;
import ru.faoxis.otusalg.sort.SorterTest;

import java.util.Random;
import java.util.stream.IntStream;

import static org.junit.Assert.*;

public class BucketSorterTest extends O1SorterTest {

    @Override
    protected Sorter getSorter() {
        return new BucketSorter(100);
    }

    @Override
    protected int getBound() {
        return 100;
    }

    @Override
    protected int getArraySize() {
        return 100_000;
    }
}