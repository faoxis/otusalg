package ru.faoxis.otusalg.sort.O1Sorters;

import ru.faoxis.otusalg.sort.Sorter;

import static org.junit.Assert.*;

public class RadixSorterTest extends O1SorterTest {

    @Override
    protected Sorter getSorter() {
        return new RadixSorter();
    }

    @Override
    protected int getBound() {
        return 10_000;
    }

    @Override
    protected int getArraySize() {
        return 100_000;
    }
}