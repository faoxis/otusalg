package ru.faoxis.otusalg.sort;

import org.junit.Test;
import ru.faoxis.otusalg.mycollection.mylist.mutable.List;
import ru.faoxis.otusalg.sort.shell.A036562ShellSorter;

import static org.junit.Assert.assertEquals;

public class A036562ShellSorterTest extends SorterTest{

    @Override
    public Sorter getSorter() {
        return new A036562ShellSorter();
    }

    @Test
    public void testGenerateGap() {
        A036562ShellSorter sorter =  new A036562ShellSorter();
        List<Integer> gaps = sorter.getGaps(300);
        assertEquals(Integer.valueOf(1), gaps.get(0));
        assertEquals(Integer.valueOf(8), gaps.get(1));
        assertEquals(Integer.valueOf(23), gaps.get(2));
        assertEquals(Integer.valueOf(77), gaps.get(3));
        assertEquals(Integer.valueOf(281), gaps.get(4));
    }
}