package ru.faoxis.otusalg.sort;

import org.junit.Ignore;
import org.junit.Test;
import ru.faoxis.otusalg.util.array.ArrayBuilder;

import java.util.Arrays;

import static org.junit.Assert.*;

@Ignore
public abstract class SorterTest {

    public abstract Sorter getSorter();
    private ArrayBuilder arrayBuilder = new ArrayBuilder();
    private int ARRAY_SIZE = 100_000;

    @Test
    public void testOnRandomArray() {
        testArray(arrayBuilder.getRandomArray(ARRAY_SIZE));
    }

    @Test
    public void testOnSortedArray() {
        testArray(arrayBuilder.getFiveElementRandomArray(ARRAY_SIZE));
    }

    @Test
    public void testOnPartlySorted() {
        testArray(arrayBuilder.getTenPercentRandomArray(ARRAY_SIZE));
    }

    private void testArray(int[] sourceArray) {
        int[] sortedArray = Arrays.copyOf(sourceArray, sourceArray.length);
        getSorter().sort(sortedArray);
        for (int i = 0; i < sortedArray.length - 1; i++) {
            if (!(sortedArray[i] <= sortedArray[i + 1])) {
                System.out.println(
                    "i=" + i + "\n" +
                    "sortedArray[i]" + sortedArray[i] + "\n" +
                    "sortedArray[i + 1]" + sortedArray[i + 1]
                );
            }
            assertTrue(sortedArray[i] <= sortedArray[i + 1]);
        }

        for (int i = 0; i < sourceArray.length; i++) {
            assertTrue(contains(sortedArray, sourceArray[i]));
        }
    }

    private boolean contains(int[] arr, int element) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == element)
                return true;
        }
        return false;
    }
}