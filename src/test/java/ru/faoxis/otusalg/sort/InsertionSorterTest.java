package ru.faoxis.otusalg.sort;


import org.junit.Test;

import static org.junit.Assert.*;

public class InsertionSorterTest extends SorterTest {

    @Override
    public Sorter getSorter() {
        return new InsertionSorter();
    }

    @Test
    public void newTest() {
        Sorter sorter = new InsertionSorter();
        int[] arr = new int[]{9, 0, 7, 0, 8, 7, 6, 8, 1, 0, 8, 0, 7};
        sorter.sort(arr);
        for (int i : arr) {
            System.out.print(i + " ");
        }
        System.out.println();
    }
}