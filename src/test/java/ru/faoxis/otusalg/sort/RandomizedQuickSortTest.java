package ru.faoxis.otusalg.sort;

public class RandomizedQuickSortTest extends SortTest {

    @Override
    protected void makeSort(int[] mass) {
        QuickSort.sortRandomPivot(mass);
    }
}
