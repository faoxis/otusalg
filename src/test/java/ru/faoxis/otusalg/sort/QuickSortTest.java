package ru.faoxis.otusalg.sort;

public class QuickSortTest extends SortTest {

    @Override
    protected void makeSort(int[] mass) {
        QuickSort.sort(mass);
    }
}