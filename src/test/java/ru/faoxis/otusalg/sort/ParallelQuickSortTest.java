package ru.faoxis.otusalg.sort;

public class ParallelQuickSortTest  extends SortTest {

    @Override
    protected void makeSort(int[] mass) throws InterruptedException {
        QuickSort.sortParallel(mass);
        Thread.sleep(100); // waiting worker executor completion
    }
}