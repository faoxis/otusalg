package ru.faoxis.otusalg.calculation.euclid;

import org.junit.Test;

import static org.junit.Assert.*;
import static ru.faoxis.otusalg.calculation.GCDAlgorithms.*;

public class GCDAlgorithmsTest {

    @Test
    public void testBySubtraction() {
        testAllRealisations(3, 180, 51);
        testAllRealisations(3, 51, 180);

        testAllRealisations(15, 1234567890, 12345);
        testAllRealisations(15, 12345, 1234567890);
    }


    private void testAllRealisations(long expected, long a, long b) {
        assertEquals(expected, gcdBySubtraction(a, b));
        assertEquals(expected, gcdByDividing(a, b));
        assertEquals(expected, binaryGcd(a, b));
    }


}