package ru.faoxis.otusalg.calculation.prime;

public class BiteShiftSievePrimeFinderTest extends PrimeFinderTest {

    @Override
    protected PrimeFinder getPrimeFinder() {
        return new BiteShiftSievePrimeFinder();
    }
}