package ru.faoxis.otusalg.calculation.prime;

import static org.junit.Assert.*;

public class FastSievePrimeFinderTest extends PrimeFinderTest {

    @Override
    protected PrimeFinder getPrimeFinder() {
        return new FastSievePrimeFinder();
    }
}