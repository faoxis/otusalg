package ru.faoxis.otusalg.calculation.prime;

import org.junit.Test;

import static org.junit.Assert.*;

public class SimplePrimeFinderTest extends PrimeFinderTest {


    @Override
    protected PrimeFinder getPrimeFinder() {
        return new SimplePrimeFinder();
    }
}