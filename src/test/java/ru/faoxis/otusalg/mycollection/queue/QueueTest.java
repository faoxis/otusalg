package ru.faoxis.otusalg.mycollection.queue;

import org.junit.Test;
import ru.faoxis.otusalg.mycollection.queue.Queue;

import static org.junit.Assert.*;

public class QueueTest {

    @Test
    public void testEnqueueDequeueIsEmpty() {
        Queue<String> queue = new Queue<>();
        assertTrue(queue.isEmpty());

        queue.enqueue("foo");
        queue.enqueue("bar");
        queue.enqueue("baz");
        assertFalse(queue.isEmpty());

        assertEquals("foo", queue.dequeue());
        assertEquals("bar", queue.dequeue());
        assertEquals("baz", queue.dequeue());
        assertTrue(queue.isEmpty());
    }


}