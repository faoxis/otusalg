package ru.faoxis.otusalg.mycollection.queue;

import org.junit.Test;

import static org.junit.Assert.*;

public class LinkedPriorityQueueTest {


    @Test
    public void basicQueueOperations() {
        PriorityQueue<String> queue = new LinkedPriorityQueue<>();
        assertTrue(queue.isEmpty());

        queue.enqueue(1, "foo");
        queue.enqueue(1, "bar");
        queue.enqueue(1, "baz");
        assertFalse(queue.isEmpty());

        assertEquals("foo", queue.dequeue());
        assertEquals("bar", queue.dequeue());
        assertEquals("baz", queue.dequeue());
        assertTrue(queue.isEmpty());
    }

    @Test
    public void priorityTest() {
        PriorityQueue<String> queue = new LinkedPriorityQueue<>();

        queue.enqueue(1, "foo");
        queue.enqueue(2, "bar");
        queue.enqueue(1, "baz");
        queue.enqueue(10, "first");

        assertFalse(queue.isEmpty());

        assertEquals("first", queue.dequeue());
        assertEquals("bar", queue.dequeue());
        assertEquals("foo", queue.dequeue());
        assertEquals("baz", queue.dequeue());
        assertTrue(queue.isEmpty());
    }

}