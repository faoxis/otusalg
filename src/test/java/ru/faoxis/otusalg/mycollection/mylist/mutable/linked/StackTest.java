package ru.faoxis.otusalg.mycollection.mylist.mutable.linked;

import org.junit.Test;
import ru.faoxis.otusalg.mycollection.Stack;

import static org.junit.Assert.*;

public class StackTest {

    @Test
    public void testIsEmpty() {
        Stack<String> stack = new Stack<>();
        assertTrue(stack.isEmpty());
    }

    @Test
    public void testPushPopIsEmpty() {
        Stack<String> stack = new Stack<>();

        stack.push("foo");
        stack.push("bar");
        stack.push("baz");

        assertFalse(stack.isEmpty());

        assertEquals("baz", stack.pop());
        assertEquals("bar", stack.pop());
        assertEquals("foo", stack.pop());

        assertTrue(stack.isEmpty());
    }
}