package ru.faoxis.otusalg.mycollection.mylist.mutable;

import java.util.function.Supplier;

public class SingleListTest extends ListTest {

    @Override
    protected <T> Supplier<List<T>> getCreateListSupplier() {
        return SingleList::new;
    }
}