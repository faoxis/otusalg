package ru.faoxis.otusalg.mycollection.mylist.mutable;

import java.util.function.Supplier;

public class VectorListTest extends ListTest {
    @Override
    protected <T> Supplier<List<T>> getCreateListSupplier() {
        return VectorList::new;
    }
}
