package ru.faoxis.otusalg.mycollection.mylist.mutable;

import java.util.function.Supplier;

public class LinkedListTest extends ListTest {

    @Override
    protected <T> Supplier<List<T>> getCreateListSupplier() {
        return LinkedList::new;
    }
}