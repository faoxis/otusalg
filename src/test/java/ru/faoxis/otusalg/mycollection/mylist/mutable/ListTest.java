package ru.faoxis.otusalg.mycollection.mylist.mutable;

import java.util.function.Supplier;

import org.junit.Ignore;
import org.junit.Test;
import static org.junit.Assert.*;

@Ignore
public abstract class ListTest {

    protected abstract <T> Supplier<List<T>> getCreateListSupplier();

    @Test
    public void testSize() {
        List<String> list = createList();
        addElementsToList(list, "foo", "bar", "buz");
        assertEquals(3, list.size());

        addElementsToList(list, "one more", "and again");
        assertEquals(5, list.size());
    }

    @Test
    public void testAddAndGet() {
        List<String> list = createList();
        addElementsToList(list, "first", "second", "third");

        assertEquals("first", list.get(0));
        assertEquals("second", list.get(1));
        assertEquals("third", list.get(2));

        list.add("one more");
        assertEquals("one more", list.get(3));
    }

    @Test
    public void testAddForBigList() {
        List<Integer> list = createList();
        int elementsNumber = 100_000;

        for (Integer i = 0; i < elementsNumber; i++) {
            list.add(i);
            assertEquals(i, list.get(i));
        }
    }

    @Test
    public void testAddElementToMiddle() {
        List<Integer> list = createList();
        for (int i = 0; i <= 100; i++) {
            list.add(i);
        }
        list.add(42, 50);

        assertEquals(Integer.valueOf(49), list.get(49));
        assertEquals(Integer.valueOf(42), list.get(50));
        assertEquals(Integer.valueOf(50), list.get(51));
        assertEquals(Integer.valueOf(100), list.get(101));
        assertEquals(102, list.size());
    }

    @Test
    public void testRemoveElement() {
        List<Integer> list = createList();
        for (int i = 0; i < 100; i++) {
            list.add(i);
        }
        assertEquals(100, list.size());
        assertEquals(100, list.size());

        Integer removed = list.remove(50);
        assertEquals(50, removed.intValue());
        assertEquals(99, list.size());

        // checking shifting
        assertEquals(99, list.get(98).intValue());
        assertEquals(51, list.get(50).intValue());
        assertEquals(49, list.get(49).intValue());
    }

    @Test
    public void testAddToStart() {
        List<Integer> list = createList();
        for (int i = 0; i < 100; i++) {
            list.add(i);
        }
        assertEquals(100, list.size());
        assertEquals(100, list.size());

        list.add(42, 0);
        assertEquals(42, list.get(0).intValue());
        assertEquals(0, list.get(1).intValue());
        assertEquals(99, list.get(100).intValue());
    }

    @Test
    public void testAddingToZeroIndex() {
        List list = createList();
        list.add("foo", 0);
        list.add("bar", 0);
        list.add("baz", 0);

        assertEquals("baz", list.get(0));
        assertEquals("bar", list.get(1));
        assertEquals("foo", list.get(2));
    }

    @Test
    public void testCheckingAddingToZero() {
        List list = createList();
        for (int i = 0; i < 100_000; i++) {
            list.add(i, 0);
        }
        assertEquals(100_000, list.size());
    }

    private <T> List<T> createList() {
        Supplier<List<T>> supplier = getCreateListSupplier();
        return supplier.get();
    }

    private <T> void addElementsToList(List<T> list, T... elems) {
        for (T elem : elems) {
            list.add(elem);
        }
    }

}