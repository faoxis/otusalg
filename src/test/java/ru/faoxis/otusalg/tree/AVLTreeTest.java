package ru.faoxis.otusalg.tree;

import junit.framework.TestCase;

public class AVLTreeTest extends TreeTest {

    @Override
    protected <K extends Comparable<K>, V> Tree<K, V> getTree() {
        return new AVLTree<>();
    }
}