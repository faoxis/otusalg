package ru.faoxis.otusalg.tree;


public class SimpleBinaryTreeTest extends TreeTest {

    @Override
    protected <K extends Comparable<K>, V> Tree<K, V> getTree() {
        return new SimpleBinaryTree<>();
    }
}
