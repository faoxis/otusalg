package ru.faoxis.otusalg.tree;

import org.junit.Ignore;
import org.junit.Test;

import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.Assert.*;

@Ignore
abstract public class TreeTest {

    abstract protected <K extends Comparable<K>, V> Tree<K, V> getTree();

    @Test
    public void testSimpleTreeLogic() {
        Tree<Integer, String> tree = getTree();
        assertEquals(0, tree.size());
        assertEquals(Optional.empty(), tree.get(42));

        tree.insert(42, "foo");
        assertEquals(1, tree.size());
        assertEquals(Optional.of("foo"), tree.get(42));

        tree.insert(42, "bar");
        assertEquals(1, tree.size());
        assertEquals(Optional.of("bar"), tree.get(42));

        tree.remove(42);
        assertEquals(Optional.empty(), tree.get(42));

    }

    @Test
    public void testAddAndRemove1000Elements() {
        Tree<Integer, String> tree = getTree();
        Random random = new Random();
        Map<Integer, String> elements = IntStream
                .generate(random::nextInt)
                .limit(1000)
                .boxed()
                .collect(Collectors.toMap(i -> i, String::valueOf));
        elements.forEach(tree::insert);

        assertEquals(elements.size(), tree.size());
        elements.forEach((key, value) -> assertEquals(Optional.of(value), tree.get(key)));

        elements.forEach((key, ignored) -> tree.remove(key));
        elements.forEach((key, ignored) -> assertEquals(Optional.empty(), tree.get(key)));
        assertEquals(0, tree.size());
    }

}