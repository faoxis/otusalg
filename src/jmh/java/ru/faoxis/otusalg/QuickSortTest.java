package ru.faoxis.otusalg;


import org.openjdk.jmh.annotations.*;
import ru.faoxis.otusalg.sort.QuickSort;

import java.util.Random;
import java.util.stream.IntStream;

@BenchmarkMode(Mode.All) // тестируем во всех режимах
@Warmup(iterations = 10) // число итераций для прогрева нашей функции
@Measurement(iterations = 100, batchSize = 10) // число проверочных итераций, причем в каждой из них будет по четыре вызова функции
public class QuickSortTest {

    @State(Scope.Thread)
    public static class RandomMassState {
        int[] mass = new Random().ints(1000, 0, 4242).toArray();
    }

    @State(Scope.Thread)
    public static class AscendingMassState {
        int[] mass = IntStream.range(0, 1000).toArray();
    }

    @State(Scope.Thread)
    public static class DecreasingMassState {
        int[] mass = IntStream.range(0, 1000).map(i -> 999 - i).toArray();
    }


    @Benchmark
    public void testWithEndPivotQuickSortWithRandomMass(RandomMassState state) {
        QuickSort.sort(state.mass);
    }

    @Benchmark
    public void testWithRandomPivotQuickSortWithRandomMass(RandomMassState state) {
        QuickSort.sortRandomPivot(state.mass);
    }

    @Benchmark
    public void testWithEndPivotQuickSortAscendingMassState(AscendingMassState state) {
        QuickSort.sort(state.mass);
    }

    @Benchmark
    public void testWithRandomPivotQuickSortWithAscendingMassState(AscendingMassState state) {
        QuickSort.sortRandomPivot(state.mass);
    }

    @Benchmark
    public void testWithEndPivotQuickSortWithDecreasingMassState(DecreasingMassState state) {
        QuickSort.sort(state.mass);
    }

    @Benchmark
    public void testWithRandomPivotQuickSortWithDecreasingMassState(DecreasingMassState state) {
        QuickSort.sortRandomPivot(state.mass);
    }



}
