package structure;

public class CoinCounter {

    public static int countCoins(int n, int[] coins) {
        int[] values = new int[n + 1];
        for (int x = 1; x <= n; x++) {
            values[x] = Integer.MAX_VALUE;
            for (int c : coins) {
                if (x - c >= 0) {
                    values[x] = Math.min(values[x], values[x - c] + 1);
                }
            }
        }
        System.out.print("Values:");
        for (int value : values) {
            System.out.print(" " + value);
        }
        System.out.println();

        return values[n];
    }

    public static void main(String[] args) {
        System.out.println(countCoins(10, new int[] {1, 3, 4}));
    }

}
