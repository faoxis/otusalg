package ru.faoxis.otusalg.tree;

public class AVLTree<K extends Comparable<K>, V> extends Tree<K, V> {

    @Override
    public void remove(K key) {
        tree = remove(tree, key);
    }

    private Node<K, V> remove(Node<K, V> node, K key) {
        if (node == null)
            return null;

        if (key.compareTo(node.getKey()) > 0) {
            Node<K, V> right = remove(node.getRight(), key);
            node.setRight(right);
            return balance(node);
        } else if (key.compareTo(node.getKey()) < 0) {
            Node<K, V> left = remove(node.getLeft(), key);
            node.setLeft(left);
            return balance(node);
        } else {
            Node<K, V> left = node.getLeft();
            Node<K, V> right = node.getRight();
            if (right == null) return left;
            if (left == null) return right;

            Node<K, V> min = getMin(right);
            min.setRight(remove(right, min.getKey()));
            min.setLeft(left);
            return balance(min);
        }
    }

    @Override
    public void insert(K key, V value) {
        tree = add(tree, key, value);
    }

    private Node<K, V> add(Node<K, V> tree, K key, V value) {
        if (tree == null)
            return new Node<>(key, value);

        Node<K, V> currentNode;
        if (key.compareTo(tree.getKey()) > 0) {
            Node<K, V> right = add(tree.getRight(), key, value);
            tree.setRight(right);
            currentNode = tree;
        } else if (key.compareTo(tree.getKey()) < 0) {
            Node<K, V> left = add(tree.getLeft(), key, value);
            tree.setLeft(left);
            currentNode = tree;
        } else {
            Node<K, V> newNode = new Node<>(key, value);
            newNode.setRight(tree.getRight());
            newNode.setLeft(tree.getLeft());
            currentNode = newNode;
        }
        return balance(currentNode);
    }

    private Node<K, V> balance(Node<K, V> node) {
        fixHeight(node);
        int bf = getBalanceFactor(node);

        if (bf == 2) {
            Node<K, V> left = node.getLeft();
            if (getBalanceFactor(left) < 0) {
                node.setLeft(rotateLeft(left));
            }
            return rotateRight(node);
        } else if (bf == -2) {
            Node<K, V> right = node.getRight();
            if (getBalanceFactor(right) > 0) {
                node.setRight(rotateRight(right));
            }
            return rotateLeft(node);
        }
        return node;
    }

    private Node<K, V> rotateRight(Node<K, V> tree) {
        Node<K, V> left = tree.getLeft();
        tree.setLeft(left.getRight());
        left.setRight(tree);
        return left;
    }

    private Node<K, V> rotateLeft(Node<K, V> tree) {
        Node<K, V> right = tree.getRight();
        tree.setRight(right.getLeft());
        right.setLeft(tree);
        return right;
    }

    private int getBalanceFactor(Node<K, V> node) {
        int leftHeight = calcHeight(node.getLeft());
        int rightHeight = calcHeight(node.getRight());

        return leftHeight - rightHeight;
    }


    private void fixHeight(Node<K, V> node) {
        node.setHeight(calcHeight(node));
    }

    private int calcHeight(Node<K, V> node) {
        if (node == null)
            return 0;

        int leftHeight = calcHeight(node.getLeft());
        int rightHeight = calcHeight(node.getRight());

        return 1 + Math.max(leftHeight, rightHeight);
    }
}
