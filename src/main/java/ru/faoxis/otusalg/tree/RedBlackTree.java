package ru.faoxis.otusalg.tree;

import ru.faoxis.otusalg.mycollection.mylist.mutable.FactorList;
import ru.faoxis.otusalg.mycollection.mylist.mutable.List;

public class RedBlackTree<K extends Comparable<K>, V> /* todo: extends Tree<K, V> */ {

    private RBNode<K, V> tree;

    public void insert(K key, V value) {
        RBNode<K, V> newNode = new RBNode<>(key, value, RBNode.Color.RED);
        tree = insert(tree, newNode);
        findAndFix(tree, newNode);
    }

    private RBNode<K, V> insert(RBNode<K, V> node, RBNode<K, V> newNode) {
        if (node == null) {
            return newNode;
        }

        RBNode<K, V> result;
        if (newNode.getKey().compareTo(node.getKey()) > 0) {
            RBNode<K, V> right = insert(node.getRight(), newNode);
            right.setParent(node);
            node.setRight(right);
            result = node;
        } else if (newNode.getKey().compareTo(node.getKey()) < 0) {
            RBNode<K, V> left = insert(node.getLeft(), newNode);
            left.setParent(node);
            node.setLeft(left);
            result = node;
        } else {
            newNode.setLeft(node.getLeft());
            newNode.setRight(node.getRight());
            newNode.setColor(RBNode.Color.BLACK);
            result = newNode;
        }
        return result;
    }

    private void findAndFix(RBNode<K, V> node, RBNode<K, V> newNode) {
        if (node.getKey().compareTo(newNode.getKey()) < 0) {
            findAndFix(node.getRight(), newNode);
        } else if (node.getKey().compareTo(newNode.getKey()) > 0) {
            findAndFix(node.getLeft(), newNode);
        } else {
            fixBalance(node);
        }
    }

    private RBNode<K, V> getMin(RBNode<K, V> node) {
        if (node.getLeft() == null)
            return node;
        else
            return getMin(node.getLeft());
    }

    private RBNode<K, V> removeMin(RBNode<K, V> node) {
        if (node.getLeft() == null)
            return null;
        else
            return removeMin(node.getLeft());
    }

    private void fixBalance(RBNode<K, V> node) {
        if (tree == node) {
            node.setColor(RBNode.Color.BLACK);
        } else if (node.getParent().getColor() == RBNode.Color.BLACK) {
            return;
        } else if (uncleColor(node) == RBNode.Color.RED) {
            changeColor(parent(node), RBNode.Color.BLACK);
            changeColor(uncle(node), RBNode.Color.BLACK);

            RBNode<K, V> grandParent = grandParent(node);
            changeColor(grandParent, RBNode.Color.RED);
            fixBalance(grandParent);
        } else if (uncleColor(node) == RBNode.Color.BLACK) {
            RBNode<K, V> lookingNode = node;
            if (!isLeft(lookingNode) && isLeft(lookingNode.getParent())) {
               RBNode<K, V> parent = lookingNode.getParent();
               RBNode<K, V> grandParent = parent.getParent();

               grandParent.setLeft(rotateLeft(parent));
               grandParent.getLeft().setParent(grandParent);

               lookingNode = parent;
            } else if (isLeft(lookingNode) && !isLeft(lookingNode.getParent())) {
                RBNode<K, V> parent = lookingNode.getParent();
                RBNode<K, V> grandParent = parent.getParent();

                grandParent.setRight(rotateRight(parent));
                grandParent.getLeft().setParent(grandParent);

                lookingNode = parent;
            }
            RBNode<K, V> parent = lookingNode.getParent();
            RBNode<K, V> grand = parent.getParent();

            parent.setColor(RBNode.Color.BLACK);
            grand.setColor(RBNode.Color.RED);

            if (isLeft(lookingNode) && isLeft(parent)) {
                Boolean isLeft = isLeft(grand);
                RBNode<K, V> grandGrand = grand.getParent();

                RBNode<K, V> rotateResult = rotateRight(grand);
                rotateResult.setParent(grandGrand);
                if (isLeft != null) {
                    if(isLeft) {
                        grandGrand.setLeft(rotateResult);
                        grandGrand.getLeft().setParent(grandGrand);
                    } else {
                        grandGrand.setRight(rotateResult);
                        grandGrand.getRight().setParent(grandGrand);
                    }
                }
            } else if (!isLeft(lookingNode) && !isLeft(lookingNode.getParent())) {
                Boolean isLeft = isLeft(grand);
                RBNode<K, V> grandGrand = grand.getParent();

                RBNode<K, V> rotateResult = rotateLeft(grand);
                rotateResult.setParent(grandGrand);
                if (isLeft != null) {
                    if(isLeft) {
                        grandGrand.setLeft(rotateResult);
                        grandGrand.getLeft().setParent(grandGrand);
                    } else {
                        grandGrand.setRight(rotateResult);
                        grandGrand.getRight().setParent(grandGrand);
                    }
                }
            }
            while (tree.getParent() != null) {
                tree = tree.getParent();
            }
        }
    }

    private RBNode<K, V> rotateLeft(RBNode<K, V> node) {
        RBNode<K, V> right = node.getRight();

        RBNode<K, V> newRight = right.getLeft();
        node.setRight(newRight);
        if (newRight != null) {
            newRight.setParent(node);
        }

        right.setLeft(node);
        node.setParent(right);

        return right;
    }

    private RBNode<K, V> rotateRight(RBNode<K, V> node) {
        RBNode<K, V> left = node.getLeft();

        RBNode<K, V> newLeft = left.getRight();
        node.setLeft(newLeft);

        if (newLeft != null) {
            newLeft.setParent(node);
        }

        left.setRight(node);
        node.setParent(left);

        return left;
    }

    private Boolean isLeft(RBNode<K, V> node) {
        RBNode<K, V> parent = node.getParent();
        if (parent == null) return null;
        return parent.getLeft() == node;
    }

    private RBNode<K, V> parent(RBNode<K, V> node) {
        return node.getParent();
    }

    private RBNode<K, V> grandParent(RBNode<K, V> node) {
        if (node.getParent() == null || node.getParent().getParent() == null)
            return null;
        return node.getParent().getParent();
    }

    private RBNode<K, V> uncle(RBNode<K, V> node) {
        RBNode<K, V> parent = node.getParent();
        RBNode<K, V> grand = node.getParent();

        RBNode<K, V> uncle;
        if (parent == null || grand == null)
            uncle = null;
        else if (parent.equals(grand.getLeft()))
            uncle = grand.getRight();
        else
            uncle = grand.getLeft();
        return uncle;
    }

    private RBNode.Color uncleColor(RBNode<K, V> node) {
        RBNode<K, V> uncle = uncle(node);
        if (uncle == null)
            return RBNode.Color.BLACK;
        else
            return uncle.getColor();
    }

    private void changeColor(RBNode<K, V> node, RBNode.Color color) {
        if (node == null) {
            if (color == RBNode.Color.RED) {
                throw new IllegalArgumentException("Null leaf can't be red");
            }
        } else {
            node.setColor(color);
        }
    }

    public void print() {
        FactorList<List<RBNode<K, V>>> levels = new FactorList<>();
        fillLevels(levels, List.of(tree));
        int maxNumberOfElement = (int) Math.pow(levels.size(), 2);
        for (int i = 0; i < levels.size(); i++) {
            List<RBNode<K, V>> level = levels.get(i);
            int startSpaces = (maxNumberOfElement - level.size()) * keyStringSize / 2;
            System.out.print(getSpaces(startSpaces));
            for (int j = 0; j < level.size(); j++) {
                RBNode<K, V> node = level.get(j);
                System.out.print(convertToString(node.getColor() + " " + node.getKey() + "(" + (node.getParent() == null ? "" : node.getParent().getKey()) +")"));
            }
            System.out.println();
        }
    }

    private int keyStringSize = 10;

    private String convertToString(String string) {
        int spaces = keyStringSize - string.length();
        return new StringBuilder(string)
            .append(getSpaces(spaces))
            .toString();
    }

    private StringBuilder getSpaces(int n) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < n; i++) {
            builder.append(" ");
        }
        return builder;
    }

    private void fillLevels(FactorList<List<RBNode<K, V>>> levels, List<RBNode<K, V>> nodes) {
        levels.add(nodes);
        FactorList<RBNode<K, V>> newLevel = new FactorList<>();
        for (int i = 0; i < nodes.size(); i++) {
            RBNode<K, V> node = nodes.get(i);
            RBNode<K, V> left = node.getLeft();
            RBNode<K, V> right = node.getRight();

            if (left != null)
                newLevel.add(left);
            if (right != null)
                newLevel.add(right);
        }
        if (newLevel.size() != 0) {
            fillLevels(levels, newLevel);
        }
    }


    public static void main(String[] args) {
        RedBlackTree<Integer, String> tree = new RedBlackTree<>();
        tree.insert(1, "1");
        tree.insert(2, "2");
        tree.insert(3, "3");
        tree.insert(4, "3");
        tree.insert(5, "3");
        tree.insert(6, "3");
        tree.print();
    }

}
