package ru.faoxis.otusalg.tree;

import ru.faoxis.otusalg.mycollection.mylist.mutable.FactorList;
import ru.faoxis.otusalg.mycollection.mylist.mutable.List;

import java.util.Optional;

public abstract class Tree<K extends Comparable<K>, V> {

    protected Node<K, V> tree;

    abstract public void insert(K key, V value);

    public Optional<V> get(K key) {
        return get(tree, key);
    }

    private Optional<V> get(Node<K, V> tree, K key) {
        if (tree == null) {
            return Optional.empty();
        }

        if (key.compareTo(tree.getKey()) > 0) {
            return get(tree.getRight(), key);
        } else if (key.compareTo(tree.getKey()) < 0) {
            return get(tree.getLeft(), key);
        } else {
            return Optional.of(tree.getValue());
        }
    }

    public int size() {
        return size(tree);
    }

    private int size(Node<K, V> node) {
        if (node == null)
            return 0;

        return 1 + size(node.getLeft()) + size(node.getRight());
    }

    public void remove(K key) {
        tree = remove(tree, key);
    }

    private Node<K, V> remove(Node<K, V> tree, K key) {
        if (tree == null)
            return null;

        if (key.compareTo(tree.getKey()) < 0) {
            Node<K, V> node = remove(tree.getLeft(), key);
            tree.setLeft(node);
            return tree;
        } else if (key.compareTo(tree.getKey()) > 0) {
            Node<K, V> node = remove(tree.getRight(), key);
            tree.setRight(node);
            return tree;
        } else {
            Node<K, V> left = tree.getLeft();
            Node<K, V> right = tree.getRight();

            if (right == null) return left;
            if (left == null) return right;

            Node<K, V> min = getMin(right);
            min.setRight(remove(right, min.getKey()));
            min.setLeft(left);
            return min;
        }
    }

    protected Node<K, V> getMin(Node<K, V> tree) {
        if (tree.getLeft() == null)
            return tree;
        else
            return getMin(tree.getLeft());
    }


    public void print() {
        FactorList<List<Node<K, V>>> levels = new FactorList<>();
        fillLevels(levels, List.of(tree));
        int maxNumberOfElement = (int) Math.pow(levels.size(), 2);
        for (int i = 0; i < levels.size(); i++) {
            List<Node<K, V>> level = levels.get(i);
            int startSpaces = (maxNumberOfElement - level.size()) * keyStringSize / 2;
            System.out.print(getSpaces(startSpaces));
            for (int j = 0; j < level.size(); j++) {
                System.out.print(convertToString(level.get(j).getKey()));
            }
            System.out.println();
        }
    }

    private int keyStringSize = 15;
    private String convertToString(K key) {
        String string = key.toString();
        int spaces = keyStringSize - string.length();
        return new StringBuilder(string)
                .append(getSpaces(spaces))
                .toString();
    }

    private StringBuilder getSpaces(int n) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < n; i++) {
            builder.append(" ");
        }
        return builder;
    }

    private void fillLevels(FactorList<List<Node<K, V>>> levels, List<Node<K, V>> nodes) {
        levels.add(nodes);
        FactorList<Node<K, V>> newLevel = new FactorList<>();
        for (int i = 0; i < nodes.size(); i++) {
            Node<K, V> node = nodes.get(i);
            Node<K, V> left = node.getLeft();
            Node<K, V> right = node.getRight();

            if (left != null)
                newLevel.add(left);
            if (right != null)
                newLevel.add(right);
        }
        if (newLevel.size() != 0) {
            fillLevels(levels, newLevel);
        }
    }


}
