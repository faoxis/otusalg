package ru.faoxis.otusalg.tree;

public class Node<K extends Comparable, V> {

    private final K key;
    private final V value;

    private Node<K, V> left;
    private Node<K, V> right;

    private int height;

    public Node(K key, V value) {
        this.key = key;
        this.value = value;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public K getKey() {
        return key;
    }

    public V getValue() {
        return value;
    }

    public Node<K, V> getLeft() {
        return left;
    }

    public void setLeft(Node<K, V> left) {
        this.left = left;
    }

    public Node<K, V> getRight() {
        return right;
    }

    public void setRight(Node<K, V> right) {
        this.right = right;
    }
}
