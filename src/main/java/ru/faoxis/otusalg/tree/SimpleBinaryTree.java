package ru.faoxis.otusalg.tree;

import ru.faoxis.otusalg.mycollection.mylist.mutable.FactorList;
import ru.faoxis.otusalg.mycollection.mylist.mutable.List;

import java.util.Random;

public class SimpleBinaryTree<K extends Comparable<K>, V> extends Tree<K, V>  {

    @Override
    public void insert(K key, V value) {
        Node<K, V> node = new Node<>(key, value);
        tree = add(tree, node);
    }

    private Node<K, V> add(Node<K, V> tree, Node<K, V> newNode) {
        if (tree == null)
            return newNode;

        if (newNode.getKey().compareTo(tree.getKey()) > 0) {
            Node<K, V> right = add(tree.getRight(), newNode);
            tree.setRight(right);
            return tree;
        } else if (newNode.getKey().compareTo(tree.getKey()) < 0) {
            Node<K, V> left = add(tree.getLeft(), newNode);
            tree.setLeft(left);
            return tree;
        } else {
            newNode.setRight(tree.getRight());
            newNode.setLeft(tree.getLeft());
            return newNode;
        }
    }


    public static void main(String[] args) {
        Random random = new Random();
        SimpleBinaryTree<Integer, String> tree = new SimpleBinaryTree<>();
        for (int i = 0; i < 100; i++) {
            int number = random.nextInt();
            tree.insert(number, String.valueOf(number));
        }

        tree.print();
    }

}
