package ru.faoxis.otusalg.tree;

import java.util.Objects;

public class RBNode<K extends Comparable<K>, V> {

    private final K key;
    private final V value;

    private Color color;
    private RBNode<K, V> parent;

    private RBNode<K, V> left;
    private RBNode<K, V> right;

    public RBNode(K key, V value, Color color) {
        this.key = key;
        this.value = value;
        this.color = color;
    }

    public K getKey() {
        return key;
    }

    public V getValue() {
        return value;
    }

    public RBNode<K, V> getLeft() {
        return left;
    }

    public void setLeft(RBNode<K, V> left) {
        this.left = left;
    }

    public RBNode<K, V> getRight() {
        return right;
    }

    public void setRight(RBNode<K, V> right) {
        this.right = right;
    }

    public Color getColor() {
        return color;
    }

    public RBNode<K, V> getParent() {
        return parent;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public void setParent(RBNode<K, V> parent) {
        this.parent = parent;
    }

    public static enum Color {
        RED,
        BLACK
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RBNode<?, ?> rbNode = (RBNode<?, ?>) o;
        return Objects.equals(key, rbNode.key) &&
            Objects.equals(value, rbNode.value) &&
            color == rbNode.color;
    }

    @Override
    public String toString() {
        return "Key: " + getKey() + " Value: " + getValue() + " Color: " + getColor() + " Parent: " + (getParent() == null ? "" : getParent().getKey());
    }

    @Override
    public int hashCode() {
        return Objects.hash(key, value, color);
    }
}
