package ru.faoxis.otusalg.mycollection;

public class Stack<T> {
    private Node<T> cursor;

    public Stack() {
        cursor = null;
    }

    public void push(T item) {
        cursor = new Node<>(item, cursor);
    }

    public boolean isEmpty() {
        return cursor == null;
    }

    public T pop() {
        if (isEmpty()) return null;
        T item = cursor.getHead();
        cursor = cursor.getTail();

        return item;
    }
}