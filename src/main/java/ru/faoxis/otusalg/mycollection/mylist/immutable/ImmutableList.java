package ru.faoxis.otusalg.mycollection.mylist.immutable;

import java.util.function.Consumer;

public interface ImmutableList<T> {

    T head();
    ImmutableList tail();
    Type getType();

    default void forEach(Consumer<T> consumer) {

    }

}
