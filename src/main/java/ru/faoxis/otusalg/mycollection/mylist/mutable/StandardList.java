package ru.faoxis.otusalg.mycollection.mylist.mutable;

abstract class StandardList<T> implements List<T> {

    private java.util.List<T> standardList = getStandardList();

    protected abstract java.util.List<T> getStandardList();

    @Override
    public int size() {
        return standardList.size();
    }

    @Override
    public void add(T item) {
        standardList.add(item);
    }

    @Override
    public T get(int index) {
        return standardList.get(index);
    }

    @Override
    public void add(T item, int index) {
        standardList.add(index, item);
    }

    @Override
    public T remove(int index) {
        return standardList.remove(index);
    }
}
