package ru.faoxis.otusalg.mycollection.mylist.mutable;

import ru.faoxis.otusalg.mycollection.Node;

public class LinkedList<T> implements List<T> {

    private Node<T> first;
    private Node<T> last;
    private int size;

    public LinkedList() {
        size = 0;
        first = null;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public void add(T item) {
        Node<T> node = new Node<>(item);
        if (first == null) {
            first = node;
            last = node;
        } else {
            last.setTail(node);
            last = node;
        }
        size++;
    }

    @Override
    public T get(int index) {
        Node<T> curr = getNode(index);
        return curr.getHead();
    }

    @Override
    public void add(T item, int index) {

        if (index == size) {
            add(item);
        } else {
            Node<T> newNode = new Node<>(item);
            if (index == 0) {
                newNode.setTail(first);
                first = newNode;
            } else {
                Node<T> beforeNode = getNode(index - 1);
                Node<T> afterNode = beforeNode.getTail();

                newNode.setTail(afterNode);
                beforeNode.setTail(newNode);
            }
            size++;
        }

    }

    private Node<T> getNode(int index) {
        Node<T> curr = first;
        for (int i = 0; i < index; i++) {
            curr = curr.getTail();
        }
        return curr;
    }

    @Override
    public T remove(int index) {
        Node<T> beforeRemovedNode = getNode(index - 1);
        Node<T> removedElement = beforeRemovedNode.getTail();
        Node<T> afterRemovedNode = removedElement.getTail();

        beforeRemovedNode.setTail(afterRemovedNode);
        size--;
        return removedElement.getHead();
    }
}
