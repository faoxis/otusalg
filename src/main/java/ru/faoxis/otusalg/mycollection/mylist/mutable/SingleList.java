package ru.faoxis.otusalg.mycollection.mylist.mutable;

public class SingleList<T> extends ArrayList<T> {

    public SingleList() {
        array = new Object[0];
    }

    public SingleList(T... elems) {
        this();
        for (T elem : elems) {
            add(elem);
        }
    }

    @Override
    public int size() {
        return array.length;
    }

    @Override
    public void add(T item) {
        resize(() -> size() + 1);
        array[size() - 1] = item;
    }

    @Override
    public void add(T item, int index) {
        resize(() -> size() + 1);
        System.arraycopy(array, index, array, index + 1, size() - index - 1);
        array[index] =  item;
    }

    @Override
    public T remove(int index) {
        T retValue = get(index);
        Object newArray[] = new Object[size() - 1];
        System.arraycopy(array, 0, newArray, 0, index);
        System.arraycopy(array, index + 1, newArray, index, size() - index - 1);
        array = newArray;
        return retValue;
    }

    @Override
    public T get(int index) {
        return (T) array[index];
    }

}
