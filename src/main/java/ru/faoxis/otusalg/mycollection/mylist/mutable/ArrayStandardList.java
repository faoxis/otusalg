package ru.faoxis.otusalg.mycollection.mylist.mutable;

import java.util.ArrayList;
import java.util.List;

public class ArrayStandardList<T> extends StandardList<T> {
    @Override
    protected List<T> getStandardList() {
        return new ArrayList<>();
    }
}
