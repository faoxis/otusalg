package ru.faoxis.otusalg.mycollection.mylist.mutable;


public interface List<T> {
    int size();
    void add(T item);
    T get(int index);
    void add(T item, int index);
    T remove(int index);

    static <T> List<T> of(T... elements) {
        List<T> list = new FactorList<>();
        for (T element : elements) {
            list.add(element);
        }

        return list;
    }

    default boolean contains(T element) {
        if (element == null) return false;
        for (int i = 0; i < size(); i++) {
            if (element.equals(get(i))) {
                return true;
            }
        }
        return false;
    }
}
