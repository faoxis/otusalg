package ru.faoxis.otusalg.mycollection.mylist.mutable;

import java.util.LinkedList;
import java.util.List;

public class LinkedStandradList<T> extends StandardList<T> {
    @Override
    protected List<T> getStandardList() {
        return new LinkedList<>();
    }
}
