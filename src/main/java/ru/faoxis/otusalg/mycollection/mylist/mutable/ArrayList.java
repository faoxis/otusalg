package ru.faoxis.otusalg.mycollection.mylist.mutable;

import java.util.Iterator;
import java.util.function.Function;
import java.util.function.Supplier;

public abstract class ArrayList<T> implements List<T>, Iterable<T> {
    protected Object[] array;

    public ArrayList() {
        this.array = new Object[512];
    }

    public ArrayList(int initSize) {
        this.array = new Object[initSize];
    }

    public ArrayList(T... elements) {
        this();
        for (T element : elements) {
            add(element);
        }
    }

    protected void resize(Supplier<Integer> increaseStrategy) {
        Object[] newArray = new Object[increaseStrategy.get()];
        System.arraycopy(array, 0, newArray, 0, size());
        array = newArray;
    }

    public <R> ArrayList<R> map(Function<T, R> function) {
        ArrayList<R> list = new FactorList<R>();
        for (int i = 0; i < size(); i++) {
            R mappedElement = function.apply(get(i));
            list.add(mappedElement);
        }
        return list;
    }

    @Override
    public Iterator<T> iterator() {
        return this.new FactorListIterator();
    }

    private class FactorListIterator implements Iterator<T> {

        int cursor;

        private FactorListIterator() {
            cursor = 0;
        }

        @Override
        public boolean hasNext() {
            return cursor != size();
        }

        @Override
        public T next() {
            return get(cursor++);
        }
    }

    public static <T> ArrayList<T> of(T... elements) {
        ArrayList<T> list = new FactorList<>();
        for (T element : elements) {
            list.add(element);
        }

        return list;
    }
}
