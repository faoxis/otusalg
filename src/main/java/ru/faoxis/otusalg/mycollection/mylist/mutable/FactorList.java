package ru.faoxis.otusalg.mycollection.mylist.mutable;

public class FactorList<T> extends ArrayList<T> {

    private Object[] array;
    private final int factor;
    private int size;

    public FactorList(int factor) {
        this.factor = factor;
        array = new Object[10];
        size = 0;
    }

    public FactorList(T... xs) {
        this();
        for (int i = 0; i < xs.length; i++) {
            add(xs[i]);
        }
    }

    public FactorList() {
        this(100);
    }




    @Override
    public int size() {
        return size;
    }

    @Override
    public void add(T item) {
        if (size ==  array.length)
            resize();
        array[size] = item;
        size++;
    }

    private void resize() {
        Object[] newArray = new Object[size * factor];
        System.arraycopy(array, 0, newArray, 0, size);
        array = newArray;
    }

    @Override
    public T get(int index) {
        return (T) array[index];
    }

    @Override
    public void add(T item, int index) {
        if (size ==  array.length)
            resize();
        System.arraycopy(array, index, array, index + 1, size - index);
        array[index] =  item;
        size++;
    }

    @Override
    public T remove(int index) {
        T retValue = get(index);
        System.arraycopy(array, index + 1, array, index, size - index - 1);
        size--;
        return retValue;
    }


}
