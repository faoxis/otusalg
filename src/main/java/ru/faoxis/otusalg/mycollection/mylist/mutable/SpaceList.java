package ru.faoxis.otusalg.mycollection.mylist.mutable;


// todo
public class SpaceList<T> extends ArrayList<T> {

    private int size;
    private int vector;
    private List<A<List<T>, Integer>> array;

    public SpaceList(int vector) {
        this.vector = vector;
        array = new SingleList<>();
        size = 0;
    }

    public SpaceList() {
        this(100);
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public void add(T item) {
        if (array.size() == 0)
            array.add(new A<>(new VectorList<>(vector), 0));

        List<T> lastList = array.get(array.size() - 1).getFirst();
        if (lastList.size() == vector){
            array.add(new A<>(new VectorList<>(vector), 0));
        }
        A<List<T>, Integer> listAndSize = array.get(size / vector);
        listAndSize.first.add(item);
        listAndSize.second = listAndSize.second + 1;
        size++;
    }

    @Override
    public T get(int index) {
        int indexAcc = 0;
        for (int i = 0; i < array.size(); i++) {
            A<List<T>, Integer> currentElement = array.get(i);
            int after = indexAcc + currentElement.second;
            if (after >= index)
                return currentElement.getFirst().get(index - indexAcc);
            indexAcc += after;
        }

        return null;
    }

    @Override
    public void add(T item, int index) {
        if (array.size() == 0)
            array.add(new A<>(new VectorList<>(vector), 0));

        int indexAcc = 0;
        int foundListIndex = 0;
        for (int i = 0; i < array.size(); i++) {
            A<List<T>, Integer> current = array.get(i);
            int size = current.getSecond();
            indexAcc += size;
            if (indexAcc > index)
                break;
            foundListIndex++;
        }

        A<List<T>, Integer> element = array.get(foundListIndex);
        List<T> lastList = element.getFirst();
        int size = element.getSecond();


        if (lastList.size() == vector){
            array.add(new A<>(new VectorList<>(vector), 0));
        }
        A<List<T>, Integer> listAndSize = array.get(size / vector);
        listAndSize.first.add(item);
        listAndSize.second = listAndSize.second + 1;
        size++;
    }

    @Override
    public T remove(int index) {
        return null;
//        int currentOuterIndex = index / vector;
//        int allOuterLists = array.size();
//        T returnValue = array
//                .get(currentOuterIndex)
//                .remove(index);
//
//        for (int i = currentOuterIndex; i < allOuterLists; i++) {
//            List<T> currentList = array.get(i);
//
//            if (currentList.size() < vector) {
//                if (i != allOuterLists - 1) {
//                    List<T> nextList = array.get(i + 1);
//                    T shiftElement = nextList.remove(0);
//                    nextList.add(shiftElement, 0);
//                }
//            }
//        }
//        size--;
//
//        return returnValue;
    }

    private static class A<H, Y> {
        private H first;
        private Y second;

        public A(H first, Y second) {
            this.first = first;
            this.second = second;
        }

        public H getFirst() {
            return first;
        }

        public void setFirst(H first) {
            this.first = first;
        }

        public Y getSecond() {
            return second;
        }

        public void setSecond(Y second) {
            this.second = second;
        }
    }
}
