package ru.faoxis.otusalg.mycollection.mylist.mutable;

public class MatrixList<T> implements List<T> {

    private int size;
    private int vector;
    private List<List<T>> array;

    public MatrixList(int vector) {
        this.vector = vector;
        array = new SingleList<>();
        size = 0;
    }

    public MatrixList() {
        this(100);
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public void add(T item) {
        if (size >= array.size() * vector) {
            array.add(new VectorList<>(vector));
        }
        array.get(size / vector).add(item);
        size++;
    }

    @Override
    public T get(int index) {
        return array.get(index / vector)
                .get(index % vector);
    }

    @Override
    public void add(T item, int index) {
        if (index == 0 && array.size() == 0) {
            add(item);
            return;
        }
        int currentOuterIndex = index / vector;
        int allOuterLists = array.size();
        array.get(currentOuterIndex).add(item, index % vector);

        boolean calledAdd = false;
        for (int i = 0; i < allOuterLists; i++) {
            List<T> currentList = array.get(i);

            if (currentList.size() > vector) {
                T shiftElement = currentList.remove(vector);

                if (i == array.size() - 1) {
                    add(shiftElement);
                    calledAdd = true;
                }
                else {
                    List<T> nextList = array.get(i + 1);
                    nextList.add(shiftElement, 0);
                }

            }
        }
        if (!calledAdd) {
            size++;
        }
    }

    @Override
    public T remove(int index) {
        int currentOuterIndex = index / vector;
        int allOuterLists = array.size();
        T returnValue = array
                .get(currentOuterIndex)
                .remove(index);

        for (int i = currentOuterIndex; i < allOuterLists; i++) {
            List<T> currentList = array.get(i);

            if (currentList.size() < vector) {
                if (i != allOuterLists - 1) {
                    List<T> nextList = array.get(i + 1);
                    T shiftElement = nextList.remove(0);
                    nextList.add(shiftElement, 0);
                }
            }
        }
        size--;

        return returnValue;
    }
}
