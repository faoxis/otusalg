package ru.faoxis.otusalg.mycollection.mylist.mutable;

public class VectorList<T> extends ArrayList<T> {

    private int size;
    private final int vector;

    public VectorList(int vector) {
        size = 0;
        array = new Object[vector];
        this.vector = vector;
    }

    public VectorList() {
        this(512);
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public void add(T item) {
        if (size == array.length) {
            resize(() -> size + vector);
        }
        array[size] = item;
        size++;
    }

    @Override
    public void add(T item, int index) {
        if (size == array.length)
            resize(() -> size + vector);
        if (size - index < 0) {
            System.out.println(item);
        }
        System.arraycopy(array, index, array, index + 1, size - index);
        array[index] =  item;
        size++;
    }

    @Override
    public T remove(int index) {
        T retValue = get(index);
        System.arraycopy(array, index + 1, array, index, size - index - 1);
        size--;
        return retValue;
    }

    @Override
    public T get(int index) {
        return (T) array[index];
    }
}
