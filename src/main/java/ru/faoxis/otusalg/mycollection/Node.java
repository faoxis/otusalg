package ru.faoxis.otusalg.mycollection;

public class Node<T> {

    private T head;
    private Node<T> tail;

    public Node(T head, Node<T> next) {
        this.head = head;
        this.tail = next;
    }

    public Node(T head) {
        this(head, null);
    }

    public T getHead() {
        return head;
    }

    public Node<T> getTail() {
        return tail;
    }

    public void setTail(Node<T> tail) {
        this.tail = tail;
    }

}
