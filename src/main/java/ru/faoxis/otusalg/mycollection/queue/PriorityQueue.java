package ru.faoxis.otusalg.mycollection.queue;

public interface PriorityQueue<T> {
    void enqueue(int priority, T item);
    T dequeue();
    boolean isEmpty();
}
