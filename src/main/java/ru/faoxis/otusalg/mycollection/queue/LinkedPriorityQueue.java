package ru.faoxis.otusalg.mycollection.queue;

public class LinkedPriorityQueue<T> implements PriorityQueue<T> {

    private final Queue[] queues;

    public LinkedPriorityQueue(int priorityMaximum) {
        queues = new Queue[priorityMaximum];
    }

    public LinkedPriorityQueue() {
        this(10);
    }
    
    @Override
    public void enqueue(int priority, T item) {
        int index = priority - 1;
        Queue queue = queues[index];
        if (queue == null) {
            queue = new Queue();
            queues[index] = queue;
        }

        queue.enqueue(item);
    }

    @Override
    public T dequeue() {
        for (int i = queues.length - 1; i >= 0; i--) {
            Queue queue = queues[i];
            if (queue != null && !queue.isEmpty()) {
                return (T) queue.dequeue();
            }
        }
        return null;
    }

    @Override
    public boolean isEmpty() {
        for (Queue queue : queues) {
            if (queue != null && !queue.isEmpty()) {
                return false;
            }
        }
        return true;
    }
}
