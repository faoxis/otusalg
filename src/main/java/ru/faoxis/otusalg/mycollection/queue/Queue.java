package ru.faoxis.otusalg.mycollection.queue;

import ru.faoxis.otusalg.mycollection.Node;

public class Queue<T> {

    private Node<T> last;
    private Node<T> first;

    public Queue() {
        last = null;
    }

    public boolean isEmpty() {
        return first == null;
    }

    public void enqueue(T item) {
        Node<T> node = new Node<>(item);
        if (isEmpty()) {
            last = node;
            first = last;
        } else {
            last.setTail(node);
            last = node;
        }
    }

    public T dequeue() {
        if (isEmpty()) {
            return null;
        }
        T retValue = first.getHead();
        first = first.getTail();
        return retValue;
    }


}
