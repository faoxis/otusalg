package ru.faoxis.otusalg.sort;

public class HeapSorter implements Sorter {
    @Override
    public void sort(int[] arr) {
        buildHeap(arr);
        for (int size = arr.length - 1; size >= 0; size--) {
            swap(arr, 0, size);
            heapify(arr, size, 0);
        }
    }

    private void buildHeap(int[] arr) {
        for (int node = arr.length / 2 - 1; node >= 0 ; node--) {
            heapify(arr, arr.length, node);
        }
    }

    private void heapify(int arr[], int size, int root) {
        int left = 2 * root + 1;
        int right = left + 1;

        int maximumIndex = root;
        if (left < size && arr[left] > arr[maximumIndex])
            maximumIndex = left;
        if (right < size && arr[right] > arr[maximumIndex])
            maximumIndex = right;
        if (maximumIndex == root) return;

        swap(arr, root, maximumIndex);
        heapify(arr, size, maximumIndex);
    }

    @Override
    public String getName() {
        return "Пирамидальная сортировка";
    }
}
