package ru.faoxis.otusalg.sort;

public interface Sorter {
    void sort(int[] arr);
    String getName();

    default void swap(int[] arr, int i, int j) {
        int tmp = arr[i];
        arr[i] = arr[j];
        arr[j] = tmp;
    }
}
