package ru.faoxis.otusalg.sort;

public class BubbleSorter implements Sorter {

    @Override
    public void sort(int[] arr) {
        for (int i = 1; i < arr.length; i++) {
            for (int j = 0; j < arr.length; j++) {
                if (arr[i] < arr[j]) {
                    swap(arr, i, j);
                }
            }
        }
    }

    @Override
    public String getName() {
        return "Пузырьковая сортировка";
    }
}
