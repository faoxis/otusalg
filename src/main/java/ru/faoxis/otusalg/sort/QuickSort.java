package ru.faoxis.otusalg.sort;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ForkJoinPool;

public class QuickSort {
    // end pivot
    public static void sort(int[] mass) {
        sort(mass, 0, mass.length - 1);
    }
    private static void sort(int[] mass, int start, int end) {
        sort(mass, start, end, false);
    }

    // random pivot
    private static Random random = new Random();
    private static int getRandomInt(int from, int until) {
        return random.nextInt((until - from) + 1) + from;
    }
    public static void sortRandomPivot(int[] mass) {
        sort(mass, 0, mass.length - 1, true);
    }

    // parallel with random pivot
    private static ExecutorService pool = ForkJoinPool.commonPool();
    public static void sortParallel(int[] mass) {
        sortParallel(mass, 0, mass.length - 1, true);
    }
    private static void sortParallel(int[] mass, int start, int end, boolean randomPivot) {
        if (start < end) {
            int pivot = getPartition(mass, start, end, randomPivot);
            pool.submit(() -> sort(mass, start, pivot - 1));
            pool.submit(() -> sort(mass, pivot, end));
        }
    }

    // general
    private static void sort(int[] mass, int start, int end, boolean randomPivot) {
        if (start < end) {
            int pivot = getPartition(mass, start, end, randomPivot);
            sort(mass, start, pivot - 1);
            sort(mass, pivot, end);
        }
    }

    private static int getPartition(int[] mass, int start, int end, boolean randomPivot) {
        if (randomPivot) {
            int randomIndex = getRandomInt(start, end);
            swap(mass, randomIndex, end);
        }
        int pivot = mass[end];

        int i = start - 1;
        for (int j = start; j < end; j++) {
            if (mass[j] <= pivot) {
                i++;
                swap(mass, i, j);
            }
        }
        i++;
        swap(mass, end, i);
        return i;
    }

    private static void swap(int[] mass, int first, int second) {
        int tmp = mass[second];
        mass[second] = mass[first];
        mass[first] = tmp;
    }

}
