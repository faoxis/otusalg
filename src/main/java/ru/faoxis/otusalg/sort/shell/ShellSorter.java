package ru.faoxis.otusalg.sort.shell;

import ru.faoxis.otusalg.mycollection.mylist.mutable.FactorList;
import ru.faoxis.otusalg.mycollection.mylist.mutable.List;
import ru.faoxis.otusalg.sort.Sorter;

import java.util.function.Function;

public abstract class ShellSorter implements Sorter {

    abstract List<Integer> getGaps(int n);
    abstract String getMethodName();

    List<Integer> generateGap(int initialCounter, int size, Function<Integer, Integer> generateNewGap) {
        List<Integer> gaps = new FactorList<>();

        int gap = 1;
        int i = 1;
        while (gap < size && gap > 0) {
            gaps.add(gap);
            gap = generateNewGap.apply(i);
            i++;
        }
        return gaps;
    }

    @Override
    public String getName() {
        return "Сортировка Шелла. Метод " + getMethodName();
    }

    @Override
    public void sort(int[] arr) {
        List<Integer> gaps = getGaps(arr.length);
        new FactorList<>();
        for (int i = 1; i < arr.length; i = 2 * i + 1) {
            gaps.add(i);
        }

        for (int gapIndex = gaps.size() - 1; gapIndex >= 0; gapIndex--) {
            for (int i = 0; i < gaps.get(gapIndex); i++) {
                insertionSort(arr, i, gaps.get(gapIndex));
            }
        }
    }

    private void insertionSort(int[] arr, int start, int gap) {
        for (int i = start; i < arr.length - 1; i += gap) {
            for (int j = Math.min(i + gap, arr.length - 1); j - gap >= 0; j = j - gap) {
                if (arr[j - gap] > arr[j]) {
                    swap(arr, j, j - gap);
                } else {
                    break;
                }
            }
        }
    }
}
