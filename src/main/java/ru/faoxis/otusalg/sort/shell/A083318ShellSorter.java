package ru.faoxis.otusalg.sort.shell;

import ru.faoxis.otusalg.mycollection.mylist.mutable.FactorList;
import ru.faoxis.otusalg.mycollection.mylist.mutable.List;

public class A083318ShellSorter extends ShellSorter {
    @Override
    public List<Integer> getGaps(int n) {
        return generateGap(1, n, k -> (int) Math.pow(2, k) + 1);
    }

    @Override
    String getMethodName() {
        return "A083318";
    }


}
