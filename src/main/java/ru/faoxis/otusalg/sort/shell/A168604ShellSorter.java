package ru.faoxis.otusalg.sort.shell;

import ru.faoxis.otusalg.mycollection.mylist.mutable.FactorList;
import ru.faoxis.otusalg.mycollection.mylist.mutable.List;

public class A168604ShellSorter extends ShellSorter {

    @Override
    List<Integer> getGaps(int n) {
        return generateGap(
                2,
                n,
                k -> (int) Math.pow(2, k) - 1
        );
    }

    @Override
    String getMethodName() {
        return "A168604";
    }
}
