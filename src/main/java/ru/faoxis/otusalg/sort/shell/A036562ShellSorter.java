package ru.faoxis.otusalg.sort.shell;

import ru.faoxis.otusalg.mycollection.mylist.mutable.List;

public class A036562ShellSorter extends ShellSorter {
    @Override
    public List<Integer> getGaps(int n) {
        return generateGap(
                1,
                n,
                k -> (int) Math.pow(4, k) + 3 * (int) Math.pow(2, k - 1) + 1
        );
    }

    @Override
    String getMethodName() {
        return "A036562";
    }
}
