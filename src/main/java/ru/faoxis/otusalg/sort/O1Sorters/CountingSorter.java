package ru.faoxis.otusalg.sort.O1Sorters;

import ru.faoxis.otusalg.sort.Sorter;

public class CountingSorter implements Sorter {

    private final int bound;

    public CountingSorter(int bound) {
        this.bound = bound;
    }

    @Override
    public void sort(int[] arr) {
        int[] counters = new int[bound];
        for (int i : arr) {
            counters[i]++;
        }

        int arrIndex = 0;
        for (int i = 0; i < counters.length; i++) {
            while (counters[i] > 0) {
                arr[arrIndex] = i;
                arrIndex++;
                counters[i]--;
            }
        }
    }

    @Override
    public String getName() {
        return "Сортировка подсчетом";
    }
}
