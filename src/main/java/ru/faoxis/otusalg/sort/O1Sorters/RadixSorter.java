package ru.faoxis.otusalg.sort.O1Sorters;

import ru.faoxis.otusalg.sort.Sorter;

import java.util.Random;
import java.util.stream.IntStream;

public class RadixSorter implements Sorter {

    public void sort(int[] arr) {
        int max = findMaxValue(arr);
        int digits = getNumberOfDigits(max);

        int divider = 1;
        for (int i = 1; i <= digits; i++) {
            sortByDigit(arr, divider);
            divider *= 10;
        }
    }

    @Override
    public String getName() {
        return "Radix sort";
    }

    private void sortByDigit(int[] arr, int divider) {
        int[] counters = new int[10];

        for (int i = 0; i < arr.length; i++) {
            int index = (arr[i] / divider) % 10;
            counters[index]++;
        }

        counters[0]--;
        for (int i = 1; i < counters.length; i++) {
            counters[i] += counters[i - 1];
        }

        int[] buffer = new int[arr.length];
        for (int i = buffer.length - 1; i >= 0; i--) {
            int index = (arr[i] / divider) % 10;
            buffer[counters[index]] = arr[i];
            counters[index]--;
        }

        System.arraycopy(buffer, 0, arr, 0, arr.length);
    }

    private int getNumberOfDigits(int number) {
        return String.valueOf(number).length();
    }

    private int findMaxValue(int[] arr) {
        int max = arr[0];
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] > max)
                max = arr[i];
        }
        return max;
    }
}
