package ru.faoxis.otusalg.sort.O1Sorters;

import ru.faoxis.otusalg.sort.InsertionSorter;
import ru.faoxis.otusalg.sort.Sorter;

public class BucketSorter implements Sorter {

    private final int bucketsCount;
    private final Sorter sorter;

    public BucketSorter(int bucketsCount, Sorter sorter) {
        this.bucketsCount = bucketsCount;
        this.sorter = sorter;
    }

    public BucketSorter(int bucketsCount) {
        this.bucketsCount = bucketsCount;
        this.sorter = new InsertionSorter();
    }

    @Override
    public void sort(int[] arr) {
        int[][] buckets = new int[this.bucketsCount][];
        int[] counters = new int[this.bucketsCount];
        int max = getMax(arr);

        for (int elem : arr) {
            int index = elem * (bucketsCount - 1) / max;
            addElement(buckets, counters, index, elem);
        }

        for (int[] bucket: buckets) {
            if (bucket != null && bucket.length > 1)
                sorter.sort(bucket);
        }

        int counter = 0;
        for (int i = 0; i < bucketsCount; i++) {
            int elementCounter = counters[i];
            int[] elements = buckets[i];

            if (elements != null) {
                for (int j = elements.length - elementCounter; j < elements.length; j++) {
                    arr[counter] = elements[j];
                    counter++;
                }
            }
        }
    }

    @Override
    public String getName() {
        return "Bucket sort";
    }

    private final static int START = 5;
    private final static double DELTA = 1.75;

    private void addElement(int[][] buckets, int[] counters, int index, int element) {
        int[] elements = buckets[index];

        if (elements == null) {
            elements = new int[START];
            elements[0] = element;
            buckets[index] = elements;
            counters[index] = 1;
        } else {
            if (counters[index] >= elements.length) {
                int[] newArr = new int[(int) (elements.length * DELTA)];
                System.arraycopy(elements, 0, newArr, 0, elements.length);
                elements = newArr;
                buckets[index] = elements;
            }

            int counter = counters[index];
            elements[counter] = element;
            counters[index] = counter + 1;
        }
    }

    private int getMax(int[] arr) {
        int max = arr[0];
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] > max) {
                max = arr[i];
            }
        }
        return max;
    }

}
