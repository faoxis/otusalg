package ru.faoxis.otusalg.sort;

public class MergeSorter implements Sorter {

    @Override
    public void sort(int[] mass) {
        sort(0, mass.length - 1, mass, new int[mass.length]);
    }

    @Override
    public String getName() {
        return "Сортировка слиянием";
    }

    private void sort(int start, int end, int[] mass, int[] buffer) {
        if (start == end)
            return;

        int middle = (end + start) / 2;
        sort(start, middle, buffer, mass);
        sort(middle + 1, end, buffer, mass);

        int leftIndex = start;
        int rightIndex = middle + 1;
        for (int i = 0; i < end - start + 1; i++) {
            int insertIndex = start + i;

            if (leftIndex > middle) {
                buffer[insertIndex] = mass[rightIndex];
            } else if (rightIndex > end) {
                buffer[insertIndex] = mass[leftIndex];
            } else if (mass[leftIndex] <= mass[rightIndex]) {
                buffer[insertIndex] = mass[leftIndex];
                leftIndex++;
            } else {
                buffer[insertIndex] = mass[rightIndex];
                rightIndex++;
            }
        }
    }


}
