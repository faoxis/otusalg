package ru.faoxis.otusalg.sort;

import ru.faoxis.otusalg.mycollection.mylist.mutable.ArrayList;

public class BucketSort implements Sorter {

    private final int range;
    private final int bucketsNumber;

    public BucketSort(int range, int bucketsNumber) {
        this.range = range;
        this.bucketsNumber = bucketsNumber;
    }

    @Override
    public void sort(int[] arr) {
        ArrayList<Integer>[] buckets = new ArrayList[arr.length / bucketsNumber];
        int max = getMaxNumber(arr);
        for (int x : arr) {
            buckets[x * bucketsNumber / max].add(x);
        }

        for (ArrayList<Integer> bucket : buckets) {
            
        }

    }

    private int getMaxNumber(int[] arr) {
        int max = arr[0];
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] > max)
                max = arr[i];
        }
        return max;
    }

    @Override
    public String getName() {
        return "Bucket sort";
    }
}
