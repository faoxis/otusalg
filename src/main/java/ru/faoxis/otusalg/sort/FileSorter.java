package ru.faoxis.otusalg.sort;


import ru.faoxis.otusalg.util.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.atomic.AtomicInteger;

public class FileSorter {

    private final String directory;
    private final String fileName;
    private final Sorter sorter;
    private final ExecutorService pool = new ForkJoinPool(2);
    private AtomicInteger fileCounter = new AtomicInteger(0);

    public FileSorter(String directory, String fileName, Sorter sorter) {
        this.directory = directory;
        this.fileName = fileName;
        this.sorter = sorter;
    }

    public String sort(int chunkSize) throws IOException {
        int files = splitFile(chunkSize);
        return mergeAllFiles(0, files);
    }

    public void clearOutputDirectory() throws IOException {
        try {
            Path path = Paths.get(directory + File.separator + "output");
            Files.walk(path)
                .map(Path::toFile)
                .forEach(File::delete);
            Files.delete(path);
        } catch (NoSuchFileException e) {
            System.out.println("This directory is not exists");
        }
    }

    private String mergeAllFiles(int from, int to) {
        int filesCount = to - from + 1;
        pool.submit(() -> System.out.println("Handling " + filesCount + " files"));
        if (filesCount == 1) {
            return String.valueOf(from);
        }
        int middle = filesCount / 2 + from;
        int lastFile = from;
        for (int i = 0; i < filesCount / 2; i++) {
            int first = from + i;
            int second = middle + i;

            int newFile = mergeFiles(first, second);
            lastFile = newFile;
            asyncDeleteFile(newFile, first);
            asyncDeleteFile(newFile, second);
        }
        return mergeAllFiles(to + (filesCount % 2 == 0 ? 1 : 0), lastFile);
    }

    private void asyncDeleteFile(int newFile, int oldFile) {
        pool.submit(() -> {
            try {
                if (newFile != oldFile) {
                    Files.delete(Paths.get(getOutputFilePath(String.valueOf(oldFile))));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    private int mergeFiles(int firstFile, int secondFile) {
        int resultFile = fileCounter.getAndIncrement();
        try (File16BitReader firstReader = new File16BitReader(getOutputFilePath(String.valueOf(firstFile)));
             File16BitReader secondReader = new File16BitReader(getOutputFilePath(String.valueOf(secondFile)));
             File16BitWriter writer = new File16BitWriter(getOutputFilePath(String.valueOf(resultFile)))) {

            LessValueResolver resolver = new LessValueResolver(firstReader, secondReader);
            while (resolver.isAvailable()) {
                writer.write(resolver.getLessValue());
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return resultFile;
    }

    private String getOutputFilePath(String fileName) {
        return directory + File.separator + "output" + File.separator + fileName;
    }

    private int splitFile(int chunkSize) throws IOException {
        String path = directory + File.separator + fileName;
        Files.createDirectory(Paths.get(directory + File.separator + "output"));
        try (File16BitReader reader = new File16BitReader(path)) {
            while (reader.isAvailable()) {
                int[] chunk = readChunk(reader, chunkSize);
                String path1 = directory + File.separator + "output" + File.separator + fileCounter.getAndIncrement();
                sortAndWrite(chunk, path1);
            }
        }
        return fileCounter.get() - 1;
    }

    private void sortAndWrite(int[] chunk, String path) {
        sorter.sort(chunk);
        try (File16BitWriter writer = new File16BitWriter(path)) {
            for (int element : chunk) {
                writer.write(element);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private int[] readChunk(File16BitReader reader, int chunkSize) throws IOException {
        int[] chunk = new int[chunkSize];
        int numberCounter = 0;
        while (numberCounter < chunkSize) {
            int number = reader.read();
            if (number >= 0) {
                chunk[numberCounter] = number;
                numberCounter++;
            } else {
                break;
            }
        }
        if (numberCounter < chunkSize - 1) {
            int[] resizedChunk = new int[numberCounter];
            System.arraycopy(chunk, 0, resizedChunk, 0, numberCounter);
            chunk = resizedChunk;
        }

        return chunk;
    }
}
