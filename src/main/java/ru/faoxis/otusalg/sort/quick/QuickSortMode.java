package ru.faoxis.otusalg.sort.quick;

public enum QuickSortMode {
    END_PIVOT,
    RANDOM_PIVOT
}
