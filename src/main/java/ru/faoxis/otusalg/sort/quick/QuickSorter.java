package ru.faoxis.otusalg.sort.quick;

import ru.faoxis.otusalg.sort.Sorter;

import java.util.Random;

public class QuickSorter implements Sorter {

    private QuickSortMode mode;
    private static Random random = new Random();

    public QuickSorter(QuickSortMode mode) {
        this.mode = mode;
    }

    public QuickSorter() {
        this(QuickSortMode.RANDOM_PIVOT);
    }

    @Override
    public void sort(int[] arr) {
        sort(arr, 0, arr.length - 1);
    }

    private void sort(int[] mass, int start, int end) {
        if (start < end) {
            int pivot = getPartition(mass, start, end);
            sort(mass, start, pivot - 1);
            sort(mass, pivot, end);
        }
    }

    private int getPartition(int[] mass, int start, int end) {
        if (mode == QuickSortMode.RANDOM_PIVOT) {
            int randomIndex = getRandomInt(start, end);
            swap(mass, randomIndex, end);
        } else {
            swap(mass, start, end);
        }
        int pivot = mass[end];

        int i = start - 1;
        for (int j = start; j < end; j++) {
            if (mass[j] <= pivot) {
                i++;
                swap(mass, i, j);
            }
        }
        i++;
        swap(mass, end, i);
        return i;
    }

    private int getRandomInt(int from, int until) {
        return random.nextInt((until - from) + 1) + from;
    }

    @Override
    public String getName() {
        return "Быстрая сортировка";
    }
}
