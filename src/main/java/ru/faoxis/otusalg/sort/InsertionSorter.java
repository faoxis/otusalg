package ru.faoxis.otusalg.sort;

public class InsertionSorter implements Sorter {
    @Override
    public void sort(int[] arr) {
        for (int i = 1; i < arr.length; i++) {
            int lessIndex = i;
            for (int j = i - 1; j >= 0 && arr[j] > arr[i]; j--) {
                lessIndex = j;
            }
            if (lessIndex != i) {
                int lessValue = arr[i];
                System.arraycopy(arr, lessIndex, arr, lessIndex + 1, i - lessIndex);
                arr[lessIndex] = lessValue;
            }
        }
    }

    @Override
    public String getName() {
        return "Сортировка вставкой";
    }
}
