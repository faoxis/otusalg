package ru.faoxis.otusalg.calculation;

public class GCDAlgorithms {

    public static long gcdBySubtraction(long x, long y) {
        while (x != y) {
            if (x > y) {
                x -= y;
            } else {
                y -= x;
            }
        }
        return x;
    }

    public static long gcdByDividing(long x, long y) {
        if (x == 0) return y;
        if (y == 0) return x;

        while(x != 0 && y != 0) {
            if (x > y) {
                x = x % y;
            } else {
                y = y % x;
            }
        }
        return x > 0 ? x : y;
    }

    public static long binaryGcd(long u, long v) {
        long shift = 0;

        if (u == 0) return v;
        if (v == 0) return u;

    /* Let shift := lg K, where K is the greatest power of 2
        dividing both u and v. */
        while (((u | v) & 1) == 0) {
            shift++;
            u >>= 1;
            v >>= 1;
        }

        while ((u & 1) == 0)
            u >>= 1;

        do {
            while ((v & 1) == 0)
                v >>= 1;

            if (u > v) {
                long t = v; v = u; u = t; // Swap u and v.
            }

            v -= u; // Here v >= u.
        } while (v != 0);

        return u << shift;

    }

}
