package ru.faoxis.otusalg.calculation.prime;


import java.util.Arrays;

public class ArraySievePrimeFinder implements PrimeFinder {

    @Override
    public int countPrimes(int n) {
        boolean[] flags = getFlags(n);
        int counter = 0;
        for (int i = 0; i < flags.length; i++) {
            if (flags[i]) {
                for (int j = i * i; j < flags.length && j > 0; j += i) {
                    flags[j] = false;
                }
                counter++;
            }
        }

        return counter;
    }

    private boolean[] getFlags(int n) {
        boolean[] flags = new boolean[n];
        Arrays.fill(flags, true);
        flags[0] = false;
        flags[1] = false;
        return flags;
    }
}
