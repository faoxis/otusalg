package ru.faoxis.otusalg.calculation.prime;

public class FastSievePrimeFinder implements PrimeFinder {

    @Override
    public int countPrimes(int n) {
        int[] pr = new int[n];
        int prCounter = 0;

        int[] lp = new int[n];

        for (int i = 2; i < n; i++) {
            if (lp[i] == 0) {
                lp[i] = i;
                prCounter = addToPr(pr, prCounter, i);
            }
            for (int j = 0; j < prCounter; j++) {
                int p = pr[j];
                int updatedIndex = p * i;
                if (updatedIndex < n && updatedIndex > 0 && p <= lp[i]) {
                    lp[updatedIndex] = p;
                } else {
                    break;
                }
            }
        }
        return prCounter;
    }


    private int addToPr(int[] arr, int counter, int element) {
        arr[counter] = element;
        counter++;
        return counter;
    }



}
