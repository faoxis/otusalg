package ru.faoxis.otusalg.calculation.prime;

public interface PrimeFinder {
    int countPrimes(int n);
}
