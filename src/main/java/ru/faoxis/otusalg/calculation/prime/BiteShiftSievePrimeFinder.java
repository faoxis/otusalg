package ru.faoxis.otusalg.calculation.prime;

import ru.faoxis.otusalg.util.BitOps;

public class BiteShiftSievePrimeFinder implements PrimeFinder {
    @Override
    public int countPrimes(int n) {
        BitOps flags = getFlags(n);
        int counter = 0;
        for (int i = 0; i < flags.getSize(); i++) {
            if (flags.get(i)) {
                for (int j = i * i; j < flags.getSize() && j > 0; j += i) {
                    flags.reset(j);
                }
                counter++;
            }
        }

        return counter;
    }

    private BitOps getFlags(int n) {
        BitOps bitOps = new BitOps(n);
        bitOps.setAll();
        bitOps.reset(0);
        bitOps.reset(1);
        return bitOps;
    }

}
