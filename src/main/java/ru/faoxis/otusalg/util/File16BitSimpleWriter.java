package ru.faoxis.otusalg.util;

import java.io.Closeable;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

public class File16BitSimpleWriter implements Closeable {

    private final OutputStream stream;

    public File16BitSimpleWriter(String path) throws IOException {
        stream = Files.newOutputStream(Paths.get(path));
    }

    public void write(int i)  {
        byte right = (byte) (0xFF & i);
        byte left = (byte) (i >> 8);

        try {
            stream.write(left);
            stream.write(right);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void close() throws IOException {
        stream.close();
    }
}
