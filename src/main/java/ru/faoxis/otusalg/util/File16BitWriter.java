package ru.faoxis.otusalg.util;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousFileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;

public class File16BitWriter implements Closeable {

    private final int bufferSize = 8 * 1024;
    private final AsynchronousFileChannel channel;

    private long position = 0;
    private ByteBuffer buffer = ByteBuffer.allocate(bufferSize);
    private Future<Integer> futureWriting = new FutureTask<Integer>(() -> 0) {{ run(); }}; // stub (start point)

    public File16BitWriter(String filePath) throws IOException {
        Path path = Paths.get(filePath);
        if (Files.exists(path)) {
            Files.delete(path);
        }
        Files.createFile(path);
        channel = AsynchronousFileChannel.open(path, StandardOpenOption.WRITE);
    }

    public void write(int i) {
        byte right = (byte) (0xFF & i);
        byte left = (byte) (i >> 8);
        buffer.put(left);
        buffer.put(right);

        if (buffer.position() == bufferSize) {
            writeBufferToFile();
            buffer = ByteBuffer.allocate(bufferSize);
        }
    }

    private void writeBufferToFile() {
        int pos = buffer.position();
        try {
            futureWriting.get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        buffer.flip();
        futureWriting = channel.write(buffer, position);
        position += pos;
    }

    @Override
    public void close() throws IOException {
        writeBufferToFile();
        try {
            futureWriting.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        channel.close();
    }
}
