package ru.faoxis.otusalg.util;

import ru.faoxis.otusalg.util.File16BitWriter;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Random;

public class FileGenerator {

    private static Random random = new Random();
    public static void createFile(String pathToFile, long size) throws URISyntaxException, IOException {
//        String path = "/home/faoxis/otusalg/bigfile";
        try(File16BitWriter writer = new File16BitWriter(pathToFile)) {
            for (long i = 0; i < size; i++) {
                writer.write(random.nextInt(0xFFFF));
            }
        }
    }


}
