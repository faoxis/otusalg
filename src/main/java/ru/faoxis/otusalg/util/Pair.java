package ru.faoxis.otusalg.util;

public class Pair<T, D> {
    public final T frist;
    public final D second;

    public Pair(T first, D second) {
        this.frist = first;
        this.second = second;
    }

    public static <T, D> Pair<T, D> of(T t, D d) {
        return new Pair<>(t, d);
    }
}
