package ru.faoxis.otusalg.util.array;

import ru.faoxis.otusalg.mycollection.mylist.mutable.List;
import ru.faoxis.otusalg.sort.Sorter;

public class ArraySorterPerformanceCalculatorResult {

    private final Sorter sorter;
    private final List<Long> randomArrayResult;
    private final List<Long> sortedArrayResult;
    private final List<Long> partySortedArrayResult;

    public ArraySorterPerformanceCalculatorResult(
            Sorter sorter,
            List<Long> randomArrayResult,
            List<Long> sortedArrayResult,
            List<Long> partySortedArrayResult) {

        this.sorter = sorter;
        this.randomArrayResult = randomArrayResult;
        this.sortedArrayResult = sortedArrayResult;
        this.partySortedArrayResult = partySortedArrayResult;
    }

    public Sorter getSorter() {
        return sorter;
    }

    public List<Long> getRandomArrayResult() {
        return randomArrayResult;
    }

    public List<Long> getSortedArrayResult() {
        return sortedArrayResult;
    }

    public List<Long> getPartySortedArrayResult() {
        return partySortedArrayResult;
    }
}
