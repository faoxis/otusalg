package ru.faoxis.otusalg.util.array;

import ru.faoxis.otusalg.mycollection.mylist.mutable.FactorList;
import ru.faoxis.otusalg.mycollection.mylist.mutable.List;
import ru.faoxis.otusalg.sort.Sorter;

import java.util.function.Function;

public class ArraySorterPerformanceCalculator {
    private ArrayBuilder arrayBuilder = new ArrayBuilder();

    private Sorter sorter;
    private Function<Integer, Integer> generateArraySize;
    private int times;

    public ArraySorterPerformanceCalculator(Sorter sorter, Function<Integer, Integer> generateArraySize, int times) {
        this.sorter = sorter;
        this.generateArraySize = generateArraySize;
        this.times = times;
    }


    public ArraySorterPerformanceCalculatorResult calculate() {
        System.out.println("Calculating part 1");
        List<Long> randomArrayResult =
                getArrayPerformance(arrayBuilder::getRandomArray);

        System.out.println("Calculating part 2");
        List<Long> sortedArrayResult =
                getArrayPerformance(arrayBuilder::getFiveElementRandomArray);

        System.out.println("Calculating part 3");
        List<Long> partlySortedArrayResult =
                getArrayPerformance(arrayBuilder::getTenPercentRandomArray);

        return new ArraySorterPerformanceCalculatorResult(sorter, randomArrayResult, sortedArrayResult, partlySortedArrayResult);
    }

    private List<Long> getArrayPerformance(Function<Integer, int[]> generateArray) {
        List<Long> results = new FactorList<>();
        for (int i = 0; i < times; i++) {
            int n = generateArraySize.apply(i);
            int[] arr = generateArray.apply(n);
            results.add(calculate(() -> sorter.sort(arr)));
        }
        return results;
    }

    private Long calculate(Runnable action) {
        long start = System.currentTimeMillis();
        action.run();
        return System.currentTimeMillis() - start;
    }


}
