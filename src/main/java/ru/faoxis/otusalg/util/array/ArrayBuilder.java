package ru.faoxis.otusalg.util.array;

import java.util.Random;
import java.util.function.Function;

public class ArrayBuilder {

    private Random random = new Random();

    public int[] getRandomArray(int n) {
        return generateArray(n, i -> random.nextInt());
    }

    public int[] getSortedArray(int n) {
        return generateArray(n, i -> i);
    }

    public int[] getFiveElementRandomArray(int n) {
        int[] arr = generateArray(n, i -> i);

        int swapCounter = 0;
        while (swapCounter < 5) {
            int first = getRandomArrayIndex(arr);
            int second = getRandomArrayIndex(arr);
            if (first != second) {
                int tmp = arr[first];
                arr[first] = arr[second];
                arr[second] = tmp;
                swapCounter++;
            }
        }
        return arr;
    }

    
    private int getRandomArrayIndex(int[] arr) {
        return Math.abs(random.nextInt(arr.length));
    }
    
    public int[] getTenPercentRandomArray(int n) {
        int everyRandomValue = (int) (n * 0.1); // 10 процентов
        return generateArray(n, i -> {
            if (i % everyRandomValue == 0) {
                return random.nextInt();
            } else {
                return i;
            }
        });
    }

    private int[] generateArray(int n, Function<Integer, Integer> generateElement) {
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = generateElement.apply(i);
        }
        return arr;
    }

    public static void main(String[] args) {
        System.out.println((int) (1000 * 0.05));
    }

}
