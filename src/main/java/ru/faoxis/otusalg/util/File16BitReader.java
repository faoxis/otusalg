package ru.faoxis.otusalg.util;

import java.io.Closeable;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousFileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class File16BitReader implements Closeable {

    private final int bufferSize = 8 * 1024;
    private AsynchronousFileChannel channel;
    private final long fileSize;
    private long totalReadCounter;

    private int readBytesCounter = 0;
    private byte[] alreadyReadBytes = new byte[0];
    private ByteBuffer buffer = ByteBuffer.allocate(bufferSize);
    private Future<Integer> futureWriting;

    public File16BitReader(String filePath) throws IOException {
        fileSize = Files.size(Paths.get(filePath));
        channel = AsynchronousFileChannel.open(Paths.get(filePath), StandardOpenOption.READ);
        futureWriting = channel.read(buffer, 0);
    }

    public int read() {
        if (!isAvailable()) {
            return -1;
        }

        if (readBytesCounter == alreadyReadBytes.length) {
            updateAlreadyReadBytes();
        }
        byte left = getByte();
        byte right = getByte();

        return (((left & 0xFF) << 8) | (right & 0xFF));
    }

    public boolean isAvailable() {
        return totalReadCounter < fileSize;
    }

    private byte getByte() {
        totalReadCounter++;
        return alreadyReadBytes[readBytesCounter++];
    }

    private void updateAlreadyReadBytes() {
        try {
            futureWriting.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        buffer.flip();
        alreadyReadBytes = new byte[buffer.limit()];
        buffer.get(alreadyReadBytes);
        buffer = ByteBuffer.allocate(bufferSize);
        futureWriting = channel.read(buffer, totalReadCounter + bufferSize);
        readBytesCounter = 0;
    }

    @Override
    public void close() throws IOException {
        channel.close();
    }
}
