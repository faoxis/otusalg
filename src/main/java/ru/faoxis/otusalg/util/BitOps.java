package ru.faoxis.otusalg.util;

import java.util.Arrays;

/**
 * Класс помогает упростить управление битами
 * Не использую BitSet из-за правила на использовать стандартные структуры
 */
public class BitOps {

    private final int[] bits;
    private final static int SHIFT = 32;
    private final int size;

    public BitOps(int size) {
        this.size = size;
        int additional = SHIFT == 0 ? 0 : 1;
        bits = new int[size / SHIFT + additional];
    }

    public void set(int n) {
        bits[n / SHIFT] |= 1L << n % SHIFT;
    }

    public boolean get(int n) {
        return (bits[n / SHIFT] & 1L << n % SHIFT) != 0;
    }

    public void reset(int n) {
        bits[n / SHIFT] &= ~(1L << n % SHIFT);
    }

    public void setAll() {
        Arrays.fill(bits, 0xFFFFFFFF);
    }

    public void resetAll() {
        Arrays.fill(bits, 0);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < size; i++) {
            builder.append(get(i) ? "1" : "0");
        }
        return builder.reverse().toString();
    }

    public int getSize() {
        return size;
    }

}
