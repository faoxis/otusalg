package ru.faoxis.otusalg.util;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

public class File16BitSimpleReader implements Closeable {

    private final InputStream stream;

    public File16BitSimpleReader(String path) throws IOException {
        stream = Files.newInputStream(Paths.get(path));
    }

    public int read() throws IOException {
        if (!isAvailable())
            return -1;
        int left = stream.read();
        int right = stream.read();

        return (((left & 0xFF) << 8) | (right & 0xFF));
    }

    public boolean isAvailable() throws IOException {
        return stream.available() > 0;
    }

    @Override
    public void close() throws IOException {
        stream.close();
    }
}
