package ru.faoxis.otusalg.util;


import java.io.IOException;

public class LessValueResolver {

    private final File16BitReader reader1;
    private final File16BitReader reader2;

    private int valueFromReader1 = -1;
    private int valueFromReader2 = -1;

    public LessValueResolver(File16BitReader reader1, File16BitReader reader2) {
        this.reader1 = reader1;
        this.reader2 = reader2;
    }

    public boolean isAvailable() throws IOException {
        return reader1.isAvailable() || reader2.isAvailable() || valueFromReader1 >= 0 || valueFromReader2 >= 0;
    }

    public int getLessValue() throws IOException {
        if (valueFromReader1 < 0 && reader1.isAvailable()) {
            valueFromReader1 = reader1.read();
        }
        if (valueFromReader2 < 0 && reader2.isAvailable()) {
            valueFromReader2 = reader2.read();
        }

        int less;
        if (valueFromReader2 < 0) {
            less = valueFromReader1;
            valueFromReader1 = -1;
        } else if (valueFromReader1 < 0) {
            less = valueFromReader2;
            valueFromReader2 = -1;
        } else if (valueFromReader1 <= valueFromReader2) {
            less = valueFromReader1;
            valueFromReader1 = -1;
        } else {
            less = valueFromReader2;
            valueFromReader2 = -1;
        }
        return less;
    }

}
