package ru.faoxis.otusalg.util;

import ru.faoxis.otusalg.mycollection.mylist.mutable.FactorList;
import ru.faoxis.otusalg.mycollection.mylist.mutable.List;

import java.util.function.Function;


public class PerformanceResultPrinter {

    private List<Pair<String, List<Long>>> resultWithLabels = new FactorList<>();
    private final int firstColumnSize;
    private final int columnSize;
    private final Function<Integer, Integer> columnTitleGenerator;

    public PerformanceResultPrinter() {
        this(40, 20, valueIndex -> (int) Math.pow(10, valueIndex + 1));
    }

    public PerformanceResultPrinter(int firstColumnSize, int columnSize) {
        this(firstColumnSize, columnSize, valueIndex -> (int) Math.pow(10, valueIndex + 1));
    }

    public PerformanceResultPrinter(int firstColumnSize, int columnSize, Function<Integer, Integer> columnTitleMeasureGenerator) {
        this.firstColumnSize = firstColumnSize;
        this.columnSize = columnSize;
        this.columnTitleGenerator = columnTitleMeasureGenerator;

    }

    public void addResult(String label, List<Long> values) {
        resultWithLabels.add(Pair.of(label, values));
    }

    public String getMarkdownTable() {
        if (resultWithLabels.size() == 0) return "";
        validateArguments(resultWithLabels);
        int iterations = resultWithLabels.get(0).second.size();

        return getTitleRows(iterations)
                .append(getRows())
                .toString();

    }

    private StringBuilder getTitleRows(int iterations) {
        StringBuilder builder = new StringBuilder();
        builder.append("|").append(repeatSymbol(" ", firstColumnSize)).append("|");
        for (int i = 0; i < iterations; i++) {
            int iteration = columnTitleGenerator.apply(i);
            String strIteration = String.valueOf(iteration);
            builder.append(strIteration).append(repeatSymbol(" ", columnSize - strIteration.length())).append("|");
        }

        builder
                .append("\n")
                .append(getFinishLine());
        return builder;
    }



    private StringBuilder getRows() {
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < resultWithLabels.size(); i++) {
            Pair<String, List<Long>> resultWithLabel = resultWithLabels.get(i);

            String label = resultWithLabel.frist;
            builder
                    .append("|")
                    .append(label)
                    .append(repeatSymbol(" ", firstColumnSize - label.length()))
                    .append("|");

            List<Long> values = resultWithLabel.second;
            for (int j = 0; j < values.size(); j++) {
                Long value = values.get(j);
                String strValue = value + "ms";

                builder
                        .append(strValue)
                        .append(repeatSymbol(" ", columnSize - strValue.length()))
                        .append("|");
            }
            builder.append("\n");
        }
        return builder;
    }

    private StringBuilder repeatSymbol(String symbol, int times) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < times; i++) {
            builder.append(symbol);
        }
        return builder;
    }

    private StringBuilder getFinishLine() {
        StringBuilder builder = new StringBuilder()
                .append("|")
                .append(repeatSymbol("-", firstColumnSize))
                .append("|");
        int columns = resultWithLabels.get(0).second.size();
        for (int i = 0; i < columns; i++) {
            builder
                    .append(repeatSymbol("-", columnSize))
                    .append("|");
        }

        return builder.append("\n");
    }

    private void validateArguments(List<Pair<String, List<Long>>> structures) {
        if (structures.size() > 0) {
            int firstSize = structures.get(0).second.size();
            for (int i = 1; i < structures.size(); i++) {
                if (firstSize != structures.get(i).second.size()) {
                    throw new IllegalArgumentException("Wrong result sizes");
                }
            }
        }

    }

}
