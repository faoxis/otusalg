package ru.faoxis.otusalg.util;

import ru.faoxis.otusalg.mycollection.mylist.mutable.FactorList;
import ru.faoxis.otusalg.mycollection.mylist.mutable.List;

import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

public class CalculationUtil {

    public static <T> List<Long> getTimeOperation(Supplier<T> createObject, BiConsumer<T, Integer> action, Function<Integer, Integer> makeIteration) {
        FactorList<Long> results = new FactorList<>();
        for (int i = 1; i <= 4; i++) {
            T handledList = createObject.get();
            int iterations = makeIteration.apply(i);
            long res = calculateOperationTime((index) -> action.accept(handledList, index), iterations);
            results.add(res);
        }
        return results;
    }

    public static long calculateOperationTime(Consumer<Integer> action, int times) {
        long start = System.currentTimeMillis();
        for (int i = 0; i < times; i++) {
            action.accept(i);
        }
        long stop = System.currentTimeMillis();
        return stop - start;
    }
}
