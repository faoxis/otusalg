package ru.faoxis.otusalg.runner;

import ru.faoxis.otusalg.mycollection.mylist.mutable.ArrayList;
import ru.faoxis.otusalg.mycollection.mylist.mutable.FactorList;
import ru.faoxis.otusalg.sort.*;
import ru.faoxis.otusalg.sort.shell.A036562ShellSorter;
import ru.faoxis.otusalg.sort.shell.A083318ShellSorter;
import ru.faoxis.otusalg.sort.shell.A168604ShellSorter;
import ru.faoxis.otusalg.util.PerformanceResultPrinter;
import ru.faoxis.otusalg.util.array.ArraySorterPerformanceCalculator;
import ru.faoxis.otusalg.util.array.ArraySorterPerformanceCalculatorResult;

import java.util.function.Function;

public class HW4BasicSortsPerformance {
    public static void main(String[] args) {
        final int startIterationCount = 100_000;
        Function<Integer, Integer> generateArraySize = index -> startIterationCount * ((int) Math.pow(10, (index)));
        PerformanceResultPrinter printer = new PerformanceResultPrinter(90, 30, generateArraySize);

        ArrayList<Sorter> testLists = new FactorList<>(

                new A168604ShellSorter(),
                new A083318ShellSorter(),
                new A036562ShellSorter()
        );

        ArrayList<ArraySorterPerformanceCalculatorResult> results = new FactorList<>();
        testLists.forEach( sorter -> {
            System.out.println(sorter.getName() + " started");

            ArraySorterPerformanceCalculator calculator = new ArraySorterPerformanceCalculator(
                    sorter,
                    generateArraySize,
                    4
            );

            ArraySorterPerformanceCalculatorResult result =  calculator.calculate();
            results.add(result);
        });


        results.forEach(result ->
            printer.addResult(result.getSorter().getName() + ". Случайный массив.", result.getRandomArrayResult())
        );
        results.forEach(result ->
            printer.addResult(result.getSorter().getName() + ". 5 случайных элементов.", result.getSortedArrayResult())
        );
        results.forEach(result ->
            printer.addResult(result.getSorter().getName() + ". 10% процентов случайных элементов.", result.getPartySortedArrayResult())
        );

        System.out.println(printer.getMarkdownTable());
    }
}
