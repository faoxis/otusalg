package ru.faoxis.otusalg.runner;

import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.function.Supplier;

import ru.faoxis.otusalg.mycollection.mylist.mutable.*;
import ru.faoxis.otusalg.util.PerformanceResultPrinter;

import static ru.faoxis.otusalg.util.CalculationUtil.getTimeOperation;


/**
 * Terrible unreadable class
 */
public class ListPerformanceCalculation {

    private static List<Supplier<List<Integer>>> ALL_LIST_CREATORS = List.of(
            FactorList::new,
            ArrayStandardList::new,
            LinkedList::new,
            LinkedStandradList::new,
            MatrixList::new,
            VectorList::new,
            SingleList::new // GC killer
    );

    public static void main(String[] args)  {
        int startIterationCount = 100;
        Function<Integer, Integer> makeIteration = index -> startIterationCount * ((int) Math.pow(10, (index)));
        PerformanceResultPrinter printer = new PerformanceResultPrinter(50, 30, makeIteration);

        fillFormatter(printer, "Adding to end", List::add, makeIteration);
        fillFormatter(printer, "Adding to middle", (list, integer) -> list.add(integer, list.size() / 2), makeIteration);
        fillFormatter(printer, "Adding to beginning", (list, integer) -> list.add(integer, 0), makeIteration);

        System.out.println(printer.getMarkdownTable());
    }

    // todo mutable argument is a bad manner in java
    private static void fillFormatter(
            PerformanceResultPrinter printer,
            String operationName,
            BiConsumer<List<Integer>, Integer> action,
            Function<Integer, Integer> makeIteration) {
        for (int i = 0; i < ALL_LIST_CREATORS.size(); i++) {
            Supplier<List<Integer>> createListFunction = ALL_LIST_CREATORS.get(i);

            String listName = createListFunction.get().getClass().getSimpleName();
            Thread thread = new Thread(() -> {
                System.out.print("Starting calculation for " + operationName + " for " + listName);
                try {
                    while (true) {
                        Thread.sleep(10000);
                        System.out.print(".");
                    }
                } catch (InterruptedException e) { }
                System.out.println("  Done!");
            });
            thread.start();

            List<Long> result = getTimeOperation(createListFunction, action, makeIteration);
            printer.addResult(operationName + " for " + listName, result);
            thread.interrupt();
        }
    }



}
