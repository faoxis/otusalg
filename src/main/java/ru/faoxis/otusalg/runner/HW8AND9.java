package ru.faoxis.otusalg.runner;

import ru.faoxis.otusalg.mycollection.mylist.mutable.ArrayList;
import ru.faoxis.otusalg.mycollection.mylist.mutable.FactorList;
import ru.faoxis.otusalg.util.FileGenerator;
import ru.faoxis.otusalg.sort.FileSorter;
import ru.faoxis.otusalg.sort.HeapSorter;
import ru.faoxis.otusalg.sort.O1Sorters.BucketSorter;
import ru.faoxis.otusalg.sort.O1Sorters.CountingSorter;
import ru.faoxis.otusalg.sort.O1Sorters.RadixSorter;
import ru.faoxis.otusalg.sort.Sorter;
import ru.faoxis.otusalg.sort.quick.QuickSorter;
import ru.faoxis.otusalg.sort.shell.A083318ShellSorter;
import ru.faoxis.otusalg.util.PerformanceResultPrinter;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.function.Supplier;

public class HW8AND9 {
    public static void main(String[] args) throws Exception {
        // HW8
//        ArrayList<Integer> smallBatchSizes = ArrayList.of(64, 128, 256, 512, 1024, 2048);
//
//        testSortersOnFile(ArrayList.of(
//            A083318ShellSorter::new,
//            QuickSorter::new,
//            HeapSorter::new
//        ), smallBatchSizes);
//
//        // HW9
        int bound = 0xFFFF;
//        testSortersOnFile(ArrayList.of(
//            () -> getBucketSorter(bound / 100),
//            () -> getBucketSorter(bound / 1_000),
//            () -> new CountingSorter(0xFFFF),
//            RadixSorter::new
//        ), smallBatchSizes);


        // additional
        int mb = 1024 * 1024 / 16; // размер батча указывается в 16 битных числах
        ArrayList<Integer> largeBatchSizes = new FactorList<>(mb, 10 * mb, 100 * mb);

        testSortersOnFile(ArrayList.of(
            A083318ShellSorter::new,
            QuickSorter::new,
            HeapSorter::new
        ), largeBatchSizes);

        testSortersOnFile(ArrayList.of(
            () -> getBucketSorter(bound / 100),
            () -> getBucketSorter(bound / 1_000),
            () -> new CountingSorter(bound),
            RadixSorter::new
        ), largeBatchSizes);
    }

    private static void testSortersOnFile(ArrayList<Supplier<Sorter>> sorters, ArrayList<Integer> batchSizes) throws IOException, URISyntaxException {
        FileGenerator.createFile("/home/faoxis/otusalg/bigfile", 1_000_000_000); // 1 млрд чисел

        PerformanceResultPrinter printer = new PerformanceResultPrinter(90, 30, batchSizes::get);
        sorters.forEach(sorter -> {
            ArrayList<Long> results = batchSizes.map(size -> {
                try {
                    return sortWithDifferentFileSize(size, sorter);
                } catch (Exception e) {
                    e.printStackTrace();
                    return 0L;
                }
            });

            printer.addResult(sorter.get().getName(), results);
        });
        System.out.println(printer.getMarkdownTable());
    }

    private static Sorter getBucketSorter(int buckets) {
        return new BucketSorter(buckets) {
            @Override
            public String getName() {
                return super.getName() + ". " + buckets + " buckets";
            }
        };
    }

    // supplier becouse of inner state of some sorters
    private static long sortWithDifferentFileSize(int batchSize, Supplier<Sorter> sorterSupplier) throws Exception {
        System.out.println("Calculating for batch size " + batchSize);
        return sortWithDifferentSorter(batchSize, sorterSupplier);
    }

    private static long sortWithDifferentSorter(int batchSize, Supplier<Sorter> sorterSupplier) throws IOException {
        Sorter sorter = sorterSupplier.get();
        System.out.println(sorter.getName() + " started!");

        FileSorter fileSorter = new FileSorter("/home/faoxis/otusalg", "bigfile", sorter);
        fileSorter.clearOutputDirectory();
        long start = System.currentTimeMillis();
        System.out.println(fileSorter.sort(batchSize));
        long result = System.currentTimeMillis() - start;
        System.out.println("it takes: " + result);
        return result;
    }
}
