package ru.faoxis.otusalg.runner;

import java.util.Random;
import java.util.stream.LongStream;

import static ru.faoxis.otusalg.calculation.GCDAlgorithms.*;
import static ru.faoxis.otusalg.util.CalculationUtil.calculateOperationTime;

public class HW3GCDPerformanceCalculation {

    private static int ITERATION_COUNT = 5_000_000;

    private static Random random = new Random();
    private static long[] preparedNumbers = LongStream
            .generate(() -> random.nextLong())
            .map(Math::abs)
            .limit(ITERATION_COUNT + 1)
            .toArray();


    public static void main(String[] args) {
        System.out.println("Random numbers was created");

        long subtraction = calculateOperationTime(
                index -> gcdBySubtraction(preparedNumbers[index], preparedNumbers[index + 1]),
                ITERATION_COUNT);
        System.out.println(subtraction);

        long res2 = calculateOperationTime(
                index -> gcdByDividing(preparedNumbers[index], preparedNumbers[index + 1]),
                ITERATION_COUNT);
        System.out.println(res2);

        long res3 = calculateOperationTime(
                index -> binaryGcd(preparedNumbers[index], preparedNumbers[index + 1]),
                ITERATION_COUNT);
        System.out.println(res3);
    }

}
