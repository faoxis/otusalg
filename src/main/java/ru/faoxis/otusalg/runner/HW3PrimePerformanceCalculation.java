package ru.faoxis.otusalg.runner;

import ru.faoxis.otusalg.calculation.prime.*;
import ru.faoxis.otusalg.mycollection.mylist.mutable.FactorList;
import ru.faoxis.otusalg.mycollection.mylist.mutable.List;
import ru.faoxis.otusalg.util.PerformanceResultPrinter;

import java.util.function.Function;
import java.util.function.Supplier;

public class HW3PrimePerformanceCalculation {

    public static void main(String[] args) {
        List<PrimeFinder> primeFinders = List.of(
                new SimplePrimeFinder(),
                new ArraySievePrimeFinder(),
                new BiteShiftSievePrimeFinder(),
                new FastSievePrimeFinder()
        );

        int startIterationCount = 250_000;
        Function<Integer, Integer> makeIteration = index -> startIterationCount * ((int) Math.pow(10, (index)));

        PerformanceResultPrinter performanceResultPrinter = new PerformanceResultPrinter(50, 30, makeIteration);
        for (int i = 0; i < primeFinders.size(); i++) {
            PrimeFinder primeFinder = primeFinders.get(i);
            List<Long> simplePrimeFinderResults = calc(primeFinder, makeIteration, 4);
            performanceResultPrinter.addResult(primeFinder.getClass().getSimpleName(), simplePrimeFinderResults);
        }

        System.out.println(performanceResultPrinter.getMarkdownTable());
    }

    private static List<Long> calc(PrimeFinder primeFinder, Function<Integer, Integer> makeIteration, int times) {
        List<Long> results = new FactorList<>();
        for (int j = 0; j < times; j++) {
            int n = makeIteration.apply(j);
            long result = calc(() -> primeFinder.countPrimes(n));
            results.add(result);
        }
        return results;
    }

    private static long calc(Supplier<Integer> action) {
        long start = System.currentTimeMillis();
        action.get();
        long stop = System.currentTimeMillis();
        return stop - start;
    }

}
