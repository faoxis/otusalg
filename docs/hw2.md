## Базовые структуры данных: массив, динамический массив, список, стек, очередь, очередь с приоритетами


### Неполный массив или очередь с приоритетом
1. Написать метод добавления и удаления элементов
    * [LinkedList](../src/main/java/ru/faoxis/otusalg/mycollection/mylist/mutable/LinkedList.java)
    * [FactorList](../src/main/java/ru/faoxis/otusalg/mycollection/mylist/mutable/FactorList.java)
    * [SingleList](../src/main/java/ru/faoxis/otusalg/mycollection/mylist/mutable/SingleList.java)
    * [VectorList](../src/main/java/ru/faoxis/otusalg/mycollection/mylist/mutable/SingleList.java)
    * [LinkedList](../src/main/java/ru/faoxis/otusalg/mycollection/mylist/mutable/LinkedList.java)
    * [MatrixList](../src/main/java/ru/faoxis/otusalg/mycollection/mylist/mutable/MatrixList.java)
    * [Тесты](../src/test/java/ru/faoxis/otusalg/mycollection/mylist/mutable)
2. Таблица сравнения производительности и выводы.
    * ###### Добавление элементов в конец списка
    |                        | 100 итераций, ms | 1 000 итераций, ms | 10 000 итераций, ms | 100 000 итераций, ms |
    |------------------------|------------------|--------------------|---------------------|----------------------|
    | FactorList             | 1                | 2                  | 10                  | 51                   |
    | Стандартный ArrayList  | 1                | 4                  | 28                  | 73                   |
    | LinkedList             | 2                | 2                  | 6                   | 150                  |
    | Стандартный LinkedList | 2                | 1                  | 5                   | 255                  |
    | MatrixList             | 1                | 3                  | 421                 | 206                  |
    | VectorList             | 0                | 2                  | 30                  | 4505                 |
    | SingleList             | 2                | 262                | 42515               | 1121258              |
    
    * ###### Добавление элементов в середину списка
    |                        | 100 итераций, ms | 1 000 итераций, ms | 10 000 итераций, ms | 100 000 итераций, ms |
    |------------------------|------------------|--------------------|---------------------|----------------------|
    | FactorList             | 3                | 20                 | 287                 | 52085                |
    | Стандартный ArrayList  | 2                | 7                  | 306                 | 51289                |
    | LinkedList             | 3                | 74                 | 5794                | 1681711              |
    | Стандартный LinkedList | 2                | 64                 | 6894                | 1687194              |
    | MatrixList             | 7                | 21                 | 1051                | 167319               |
    | VectorList             | 1                | 2                  | 229                 | 46413                |
    | SingleList             | 18               | 27                 | 88690               | 1349606              |
    
    * ###### Добавление элементов в начало списка 
    |                        | 100 итераций, ms | 1 000 итераций, ms | 10 000 итераций, ms | 100 000 итераций, ms |
    |------------------------|------------------|--------------------|---------------------|----------------------|
    | FactorList             | 1                | 23                 | 624                 | 65481                |
    | Стандартный ArrayList  | 2                | 7                  | 594                 | 64078                |
    | LinkedList             | 1                | 7                  | 55                  | 744                  |
    | Стандартный LinkedList | 1                | 8                  | 69                  | 756                  |
    | MatrixList             | 1                | 29                 | 2652                | 479979               |
    | VectorList             | 0                | 17                 | 1319                | 79929                |
    | SingleList             | 0                | 34                 | 20236               | 1398075              |
    
   * ###### Вывод
   По результатам вычислений можно с легкостью понять, что реализация списка FactorList является наиболее быстрой в случае вставки в середину или конец.
   Однако LinkedList выигрывает в случае со вставкой в начало за счет отсутствия необходимости передвигать элементы массива.
   
   * [Расчет](../src/main/java/ru/faoxis/otusalg/runner/ListPerformanceCalculation.java)
    
3. Приоритетная очередь
    * [Реализация](../src/main/java/ru/faoxis/otusalg/mycollection/queue/LinkedPriorityQueue.java)
    * [Тесты](../src/test/java/ru/faoxis/otusalg/mycollection/queue/LinkedPriorityQueueTest.java)


#### [Назад](../README.md)